# Python
import os
import json
import posixpath
import mimetypes
import django_heroku
import sys
from typing import Tuple, List, Dict, Set

# 3rd Party
import requests

def extract_db_credentials_from_heroku_db_url(db_url:str) -> Tuple[str, str, str, str]:
   without_prefix = db_url[11:]
   user, rest = without_prefix.split(':', 1)
   password, rest = rest.split('@', 1)
   host, rest = rest.split(':', 1)
   port, db_name = rest.split('/')
   
   return user, password, host, port, db_name


SUPPORTED_CHAINS: Dict[str, int] = {
    'POLYGON': 137
}

mimetypes.add_type("text/css", ".css", True)
mimetypes.add_type("application/javascript", ".js", True)

# Important paths
BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
SECRETS_DIR = os.path.join(BASE_DIR, '.secrets')

# Addresses for error mails
DEFAULT_ADMIN_EMAIL = 'alvinsartor@gmail.com'
ADMINS = [('Alvin', 'alvinsartor@gmail.com')]
SERVER_EMAIL = 'alvinsartor@gmail.com'

# Load environment variables
ENVIRONMENT_VARS_FILE = os.path.join(SECRETS_DIR, 'environment.json')
if os.path.exists(ENVIRONMENT_VARS_FILE):
    # IF File exists, we get them from there,
    with open(ENVIRONMENT_VARS_FILE, 'r') as env_file:
        data = env_file.read()

    ENVIRONMENT_VARIABLES = json.loads(data)
    INSTANCE_NAME = ENVIRONMENT_VARIABLES['INSTANCE_NAME']
    DEBUG = ENVIRONMENT_VARIABLES.get('DEBUG', 'True') == 'True'
    
    SECRET_KEY = ENVIRONMENT_VARIABLES['SECRET_KEY']
    BLOCKCHAIN_NAME = ENVIRONMENT_VARIABLES['BLOCKCHAIN']
    
    TEST_WALLET_PRIVATE_KEY_1 = ENVIRONMENT_VARIABLES['TEST_WALLET_PRIVATE_KEY_1']
    TEST_WALLET_ADDRESS_1 = ENVIRONMENT_VARIABLES['TEST_WALLET_ADDRESS_1']
    TEST_WALLET_PRIVATE_KEY_2 = ENVIRONMENT_VARIABLES['TEST_WALLET_PRIVATE_KEY_2']
    TEST_WALLET_ADDRESS_2 = ENVIRONMENT_VARIABLES['TEST_WALLET_ADDRESS_2']

    DB_NAME = ENVIRONMENT_VARIABLES['DB_NAME']
    DB_USER = ENVIRONMENT_VARIABLES['DB_USER']
    DB_HOST = ENVIRONMENT_VARIABLES['DB_HOST']
    DB_PORT = ENVIRONMENT_VARIABLES['DB_PORT']
    DB_PASSWORD = ENVIRONMENT_VARIABLES['DB_PASSWORD']
    DB_KEEP_MINIMAL = ENVIRONMENT_VARIABLES.get('DB_KEEP_MINIMAL', 'False') == 'True'
    DEFAULT_ADMIN_PASSWORD = ENVIRONMENT_VARIABLES['DEFAULT_ADMIN_PASSWORD']
    SSB_SECURITY_KEY = ENVIRONMENT_VARIABLES['SSB_SECURITY_KEY']    
    HIVE_URL = ENVIRONMENT_VARIABLES['HIVE_URL'] 

    EMAIL_BACKEND = 'django.core.mail.backends.console.EmailBackend'
else:
    # ELSE from ENV
    INSTANCE_NAME = os.environ.get('INSTANCE_NAME', 'Forgotten Puma')
    DEBUG = os.environ.get('DEBUG', 'False') == 'True'
    SECRET_KEY = os.environ.get('SECRET_KEY', None)
    BLOCKCHAIN_NAME = os.environ.get('BLOCKCHAIN', 'POLYGON')

    DB_URL = os.environ.get('DATABASE_URL', None)
    DB_USER, DB_PASSWORD, DB_HOST, DB_PORT, DB_NAME = extract_db_credentials_from_heroku_db_url(DB_URL)    
    DB_KEEP_MINIMAL = os.environ.get('DB_KEEP_MINIMAL', 'False') == 'True'
    DEFAULT_ADMIN_PASSWORD = os.environ.get('DEFAULT_ADMIN_PASSWORD', None)
    SSB_SECURITY_KEY = os.environ.get('SSB_SECURITY_KEY', '')    
    HIVE_URL = os.environ.get('HIVE_URL', '') 

    EMAIL_BACKEND = 'django.core.mail.backends.smtp.EmailBackend'
    EMAIL_HOST = os.environ.get('MAILGUN_SMTP_SERVER', '')
    EMAIL_PORT = os.environ.get('MAILGUN_SMTP_PORT', '')
    EMAIL_HOST_USER = os.environ.get('MAILGUN_SMTP_LOGIN', '')
    EMAIL_HOST_PASSWORD = os.environ.get('MAILGUN_SMTP_PASSWORD', '')    

if not SECRET_KEY:
    raise Exception('Could not load environment variables')

if not HIVE_URL:
    raise Exception('I need the hive to startup!')

if BLOCKCHAIN_NAME not in SUPPORTED_CHAINS:
    raise Exception("Chain '%s' is not supported" % BLOCKCHAIN_NAME)    
BLOCKCHAIN_ID = SUPPORTED_CHAINS[BLOCKCHAIN_NAME]

TEST = 'test' in sys.argv

print("Debug mode: %s" % DEBUG)
print("Test mode: %s" % TEST)

# Allowed hosts
ALLOWED_HOSTS = ['localhost', '*.herokuapp.com', 'stableswapperbot.com']

# Application references
INSTALLED_APPS = [
    'accounts',
    'sharedSettings',
    'kernel',
    'blockchainProxy',
    'ammProxy',
    'bot',
    'statistics',
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'debug_toolbar',
]
AUTH_USER_MODEL = 'accounts.user'

# Middleware framework
MIDDLEWARE = [
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'StableSwapperAsAService.middlewares.BasicAuthenticationMiddleware',
]

if DEBUG:
    MIDDLEWARE += [
        'debug_toolbar.middleware.DebugToolbarMiddleware',
        'StableSwapperAsAService.middlewares.QueryCountDebugMiddleware',
    ]

    INTERNAL_IPS = ['127.0.0.1']
  
ROOT_URLCONF = 'StableSwapperAsAService.urls'

# Template configuration
TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
                'StableSwapperAsAService.settings_context_processor.enriched_context',
            ],
        },
    },
]

WSGI_APPLICATION = 'StableSwapperAsAService.wsgi.application'

# Database
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': DB_NAME,
        'USER': DB_USER,
        'PASSWORD': DB_PASSWORD,
        'HOST': DB_HOST,
        'PORT': DB_PORT,
    }
}

# Password validation
AUTH_PASSWORD_VALIDATORS = [
    {'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator'},
    {'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator'},
    {'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator'},
    {'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator'},
]

# Internationalization
LANGUAGE_CODE = 'en-us'
TIME_ZONE = 'UTC'
USE_I18N = True
USE_L10N = True
USE_TZ = True

# Static files (CSS, JavaScript, Images)
STATIC_URL = '/static/'
STATIC_ROOT = posixpath.join(*(BASE_DIR.split(os.path.sep) + ['static']))

# Models
DEFAULT_AUTO_FIELD = 'django.db.models.AutoField'

django_heroku.settings(locals())
