# Django
from django.conf import settings
from django.db import connection
from django.http import HttpResponse, HttpRequest, HttpResponseRedirect, JsonResponse
from django.utils.translation import ugettext as _


class BasicAuthenticationMiddleware:
    """ 
        Only allows authenticated users or request containing 
        a predefined SSB-SECURITY-KEY to perform actions.
    """
    
    def __init__(self, get_response):
        self.get_response = get_response

    def __call__(self, request: HttpRequest) -> HttpResponse:        
        # If the URL is not an action, we skip the check
        if '/action/' not in request.path:
            return self.get_response(request)
        
        # Admins can execute any request
        if request.user.is_authenticated and request.user.is_staff:
            return self.get_response(request)
        
        # Otherwise let's check if the request contains the security key
        if self._request_contains_security_token(request):
            return self.get_response(request)
        
        return JsonResponse({'outcome': 'error! You are not authenticated to perform such action'}, status=403)

    def _request_contains_security_token(self, request: HttpRequest) -> bool:
        request_security_key = request.headers.get('SSB-SECURITY-KEY', None)
        return request_security_key == settings.SSB_SECURITY_KEY



class QueryCountDebugMiddleware:
    """ 
        Middleware used to count number of queries and time used for each request. 
        It is activated only in DEBUG mode. 
    """

    MAX_NUMBER_OF_QUERIES_BEFORE_OF_SHAME_PAGE = 20
    
    def __init__(self, get_response):
        self.get_response = get_response
        
    def __call__(self, request):
        response = self.get_response(request)

        # skip count for unsuccessful responses
        if response.status_code != 200:
            return response
        
        total_time = 0

        for query in connection.queries:
            query_time = query.get('time')
            if query_time is None:

                query_time = query.get('duration', 0) / 1000
            total_time += float(query_time)
            
        # print ('** MIDDLEWARE QUERYCOUNT ** %s queries run, total %s seconds' % (len(connection.queries), total_time))
        
        if len(connection.queries) > self.MAX_NUMBER_OF_QUERIES_BEFORE_OF_SHAME_PAGE:
            html = self.compose_html(connection.queries, total_time)
            return HttpResponse(html)
        
        return response

    def compose_html(self, queries, total_time):
        return """
            <html>
                <body>
                <div class="inefficient-middleware-text">
                    OMG! You this page is terribly inefficient!                         <br/>
                    It did %s queries and it took %s seconds                            <br/><br/>
                    SHAME ON WHO WROTE THIS S***T!                                      <br/><br/><br/>
                    <img src="https://i.giphy.com/media/m6tmCnGCNvTby/giphy.webp"/>     <br/><br/><br/>
                    %s
                    </div>
                </body>
            </html>
        """ % (len(queries), total_time, '<br/><br/>'.join([query['sql'] for query in queries]))