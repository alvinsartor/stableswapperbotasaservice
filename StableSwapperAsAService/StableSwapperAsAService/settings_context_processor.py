
from django.conf import settings

def enriched_context(request):
    return {
        'HIVE_URL': settings.HIVE_URL
    }