# Django
from django.urls import path, include
from django.contrib import admin
from django.conf.urls.static import static
from django.conf import settings
from django.views.generic import RedirectView

# 3rd Party
import debug_toolbar

# fmt: off
urlpatterns = [

    path('admin/', admin.site.urls, name="admin"),
    
    path('', RedirectView.as_view(pattern_name='bots:home'), name='landing-page'),
    path('bot/', include('bot.urls')),
    path('statistics/', include('statistics.urls')),

] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)

if settings.DEBUG:
    urlpatterns += [
        path('__debug__/', include(debug_toolbar.urls)),
        ]

# fmt: on
