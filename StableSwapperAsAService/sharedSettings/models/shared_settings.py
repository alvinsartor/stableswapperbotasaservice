# Python
from typing import Dict

# Django
from django.db import models


class SharedSettings(models.Model):
    name = models.CharField(max_length=64)
    value = models.CharField(max_length=64)

    class Meta:
        verbose_name_plural = "Instance Public Settings"

    DEFAULT_SETTINGS = {
        'SWAP_THRESHOLD': '0.2',
        'MINIMUM_SWAPPABLE': '20.0',
        'MAXIMUM_SWAPPABLE_AMOUNT': '75.0',
        'MAXIMUM_GAS_PRICE': '15.1',
        'SLIPPAGE': '0.4',
        'CHECK_RUNNER_ITERATIONS': '250',
        'MINIMUM_GAS_BEFORE_TOP_UP': '0.75',
        'GAS_REFERENCE_LEVEL': 'fast',
        'EXCHANGE': 'PARASWAP',
    }
    
    @staticmethod
    def get(setting_name: str) -> Dict[str, str]:
        try:
            return SharedSettings.objects.get(name=setting_name).value
        except SharedSettings.DoesNotExist:
            default_value = SharedSettings.DEFAULT_SETTINGS[setting_name]
            SharedSettings.objects.create(name=setting_name, value=default_value)
            return default_value
        except SharedSettings.MultipleObjectsReturned:
            duplicate_settings = SharedSettings.objects.filter(name=setting_name)
            result = duplicate_settings[0].value
            duplicate_settings[1].delete()
            return result