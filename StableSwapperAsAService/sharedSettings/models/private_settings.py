# Python
from typing import Dict
from functools import lru_cache

# Django
from django.db import models
from django.conf import settings

# 3rd Party
from fernet_fields import EncryptedTextField
import requests


class PrivateSettings(models.Model):
    """ Settings containing keys or other sensitive data """

    name = models.CharField(max_length=64, unique=True)
    value = EncryptedTextField()
        
    class Meta:
        verbose_name_plural = "Instance Private Settings"

    @staticmethod
    @lru_cache(maxsize=1)
    def _retrieve_private_settings_from_hive() -> Dict[str, str]:
        # Retrieve instance settings from hive
        url = "%s/secrets/instance-private-settings/" % settings.HIVE_URL
        headers = {'SSB-SECURITY-KEY': settings.SSB_SECURITY_KEY}
        try:
            response = requests.get(url, headers=headers)
        except Exception as ex:
            raise Exception('I could not retrieve the settings from the hive')

        if response.status_code != 200:
            raise Exception('I could not retrieve the settings from the hive')
        
        return response.json()       
    

    @staticmethod
    def get(setting_name: str) -> Dict[str, str]:
        try:
            return PrivateSettings.objects.get(name=setting_name).value
        except PrivateSettings.DoesNotExist:
            default_settings = PrivateSettings._retrieve_private_settings_from_hive()
            default_value = default_settings[setting_name]
            PrivateSettings.objects.create(name=setting_name, value=default_value)
            return default_value