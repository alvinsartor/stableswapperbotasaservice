# Django
from django.contrib import admin

# Local
from sharedSettings.models import SharedSettings
from sharedSettings.models import PrivateSettings


@admin.register(SharedSettings)
class SharedSettingsAdmin(admin.ModelAdmin):
    list_display = ('name', 'value')


@admin.register(PrivateSettings)
class PrivateSettingsAdmin(admin.ModelAdmin):
    list_display = ('name', 'value')

