
# Django
from django.test import TestCase

# Local
from sharedSettings.models import SharedSettings


class SharedSettingsTest(TestCase):

    def test_existing_setting_is_retrieved(self) -> None:
        SharedSettings.objects.create(name='foo', value='bar')
        retrieved = SharedSettings.get('foo')

        self.assertEqual(retrieved, 'bar')
        
    def test_non_existing_setting_is_set_to_default_when_fetched(self) -> None:
        # SWAP_THRESHOLD setting does *not* exist
        self.assertFalse(SharedSettings.objects.filter(name='SWAP_THRESHOLD').exists())

        # Default value is used        
        retrieved = SharedSettings.get('SWAP_THRESHOLD')
        self.assertEqual(retrieved, '0.1')

        # And setting is created
        self.assertTrue(SharedSettings.objects.filter(name='SWAP_THRESHOLD').exists())
