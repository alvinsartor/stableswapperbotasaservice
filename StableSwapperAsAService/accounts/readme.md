# Accounts

Accounts are used only by admins to control the bot and make sure everything works as expected. 

Admins can also access to the admin panel and modify the `sharedSettings`, that are accessed by the bot during runtime.

Non-admin users do **not** have accounts.