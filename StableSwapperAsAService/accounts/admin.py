
# Django
from django.contrib import admin
from django.contrib.auth.admin import UserAdmin

# Local
from accounts.models import User

admin.site.register(User, UserAdmin)
