# Python
from typing import List, Dict, Optional, Any
import time
import json
import traceback

# Django
from django.utils import timezone
from django.db.models import Sum

# 3rd Party
import requests
from web3 import Web3

# Local
from kernel.models import Token
from kernel.models import Transaction
from kernel.models import Wallet
from sharedSettings.models import SharedSettings
from ammProxy.exchange import ExchangeProxyV2
from blockchainProxy.wallet_checker import WalletChecker
from blockchainProxy.token_with_amount import TokenWithAmount
from blockchainProxy.quote_with_tx_data import QuoteWithTxData
from blockchainProxy.transactions_signer import TransactionsSigner


class SwapperV3:
    @staticmethod
    def swap_if_possible(wallet: Wallet, web3: Web3) -> bool:
        owned_tokens: List[TokenWithAmount] = WalletChecker.get_tokens_amounts(wallet.address)
        total_owned = sum([token.adjusted_amount for token in owned_tokens])
        minimum_swappable = float(SharedSettings.get('MINIMUM_SWAPPABLE'))

        # Remove amounts locked in pending txs from the owned amounts
        amounts_in_pending_txs = SwapperV3._get_pending_from_amounts(wallet)
        unlocked_tokens = [t.with_adjusted_amount(max(t.adjusted_amount - amounts_in_pending_txs.get(t.token.symbol, 0), 0)) for t in owned_tokens]

        # Consider only tokens that have more than the minimum amount available to swap
        swappable_tokens = [t for t in unlocked_tokens if t.adjusted_amount >= minimum_swappable]
        swappable_tokens = sorted(swappable_tokens, key=lambda tk: -tk.adjusted_amount)
        
        # Skip tokens that are requesting for an allowance to the swap contract right now
        requesting_allowance = set(Transaction.objects.filter(type=Transaction.Type.ALLOWANCE, state=Transaction.States.WAITING, wallet=wallet).values_list('token_from__symbol', flat=True))
        swappable_tokens = [t for t in swappable_tokens if t.token.symbol not in requesting_allowance]

        # By always swapping a fixed amount we expose ourselves to more opportunities, reduce slippage issues
        # and have less chance of cornering ourselves in a bad swap
        maximum_swappable_amount = float(SharedSettings.get('MAXIMUM_SWAPPABLE_AMOUNT'))

        # Calculate how much token is allowed to receive to not pose too much risk on the portfolio
        tokens_allowances: Dict[str, float] = SwapperV3._get_allowances(owned_tokens, total_owned, wallet)

        # calculate gas price and slippage
        gas_price = SwapperV3._get_gas_price()
        slippage = float(SharedSettings.get('SLIPPAGE'))

        has_swapped = False
        for token_to_swap in swappable_tokens:
            swappable_amount = min(token_to_swap.adjusted_amount, maximum_swappable_amount)
            token_to_swap.set_amount_from_adjusted(swappable_amount)
            swapped_to = SwapperV3._swap_single_token_if_possible(
                wallet, token_to_swap, total_owned, tokens_allowances, gas_price, slippage, web3
            )

            # If something got swapped, let's modify the allowances
            if swapped_to:
                tokens_allowances[swapped_to.token.symbol] -= swapped_to.adjusted_amount
                has_swapped = True

        return has_swapped

    @staticmethod
    def _swap_single_token_if_possible(
        wallet: Wallet,
        token_to_swap: TokenWithAmount,
        total_owned: float,
        tokens_allowances: Dict[str, float],
        gas_price: float,
        slippage: float,
        web3: Web3,
    ) -> Optional[TokenWithAmount]:
        from bot.models import Check

        check = Check()
        check.wallet = wallet
        start_time = time.time()

        try:
            # store token to swap into check
            check.total_owned = total_owned
            check.token_to_swap_from = token_to_swap.token
            check.amount_to_swap_from = token_to_swap.adjusted_amount

            # get the list of tokens we can swap to considering how much they already have and their risk factor
            # if a swap of token_to_swap.amount would get past the token allowance, we discard it
            other_tokens = Token.objects.exclude(symbol=token_to_swap.token.symbol).exclude(disabled=True)
            swappable_tokens = [
                token
                for token in other_tokens
                if tokens_allowances.get(token.symbol, 0) > token_to_swap.adjusted_amount
            ]

            # get quote for allowed token
            quote_with_tx_data, check_errors = ExchangeProxyV2.get_best_swap_quote(
                wallet, token_to_swap, swappable_tokens, gas_price, slippage, web3
            )
            token_to = quote_with_tx_data.token_with_amount
            check.token_to_swap_to = token_to.token
            check.amount_to_swap_to = token_to.adjusted_amount
            check.error = check_errors

            # calculate forecasted profits
            forecasted_profit = token_to.adjusted_amount - token_to_swap.adjusted_amount
            swap_threshold = float(SharedSettings.get('SWAP_THRESHOLD'))
            aborted = forecasted_profit < swap_threshold
            check.raw_data = json.dumps(quote_with_tx_data.tx_data)
            check.forecasted_profit = forecasted_profit
            check.aborted = aborted

            if aborted:
                check.duration_seconds = time.time() - start_time
                check.save()
                return None

            # Execute transaction
            tx_address = TransactionsSigner.sign_and_submit(quote_with_tx_data, wallet, web3)

            # store successful transaction (it will be checked via a cronjob)
            transaction = Transaction.objects.create(
                wallet=wallet,
                token_from=token_to_swap.token,
                token_to=token_to.token,
                amount_from=token_to_swap.adjusted_amount,
                amount_to=token_to.adjusted_amount,
                address=tx_address,
                endpoint_uri=web3.provider.endpoint_uri[:40],
                gas_price=gas_price,
                slippage=slippage,
                type=Transaction.Type.SWAP,
                quotation_to_creation_delay=(timezone.now() - quote_with_tx_data.date).total_seconds(),
            )
            check.transaction = transaction

        except Exception as error:
            check.error += str(error)
            check.error += "\n\nstacktrace:\n" + traceback.format_exc()
            check.failed = True
            token_to = None

        check.duration_seconds = time.time() - start_time
        check.save()
        return token_to

    @staticmethod
    def _get_pending_from_amounts(wallet: Wallet) -> Dict[str, float]:
        """ 
            Analyzes the currently pending transactions and sums the amount_from per token.
            example:
            { "DAI":123, "USDC":100 } <- amounts currently in pending txs
        """
        
        pending_tokens: Dict[str, Any] = (
            Transaction.objects
            .filter(state=Transaction.States.WAITING, wallet=wallet)
            .select_related('token_from')
            .values('token_from__symbol')
            .annotate(pending=Sum('amount_from'))
        )
        return {item['token_from__symbol']:item['pending'] for item in pending_tokens}
    
    @staticmethod
    def _get_pending_to_amounts(wallet: Wallet) -> Dict[str, float]:
        """ 
            Analyzes the currently pending transactions and sums the amount_to per token.
            example:
            { "DAI":123, "USDC":100 } <- amounts currently in pending txs
        """
        
        pending_tokens: Dict[str, Any] = (
            Transaction.objects
            .filter(state=Transaction.States.WAITING, wallet=wallet)
            .select_related('token_to')
            .values('token_to__symbol')
            .annotate(pending=Sum('amount_to'))
        )
        return {item['token_to__symbol']:item['pending'] for item in pending_tokens}

    @staticmethod
    def _get_gas_price() -> float:
        max_gas_price = float(SharedSettings.get('MAXIMUM_GAS_PRICE'))
        gas_reference_level = SharedSettings.get('GAS_REFERENCE_LEVEL')
        try:
            response = requests.get('https://gasstation-mainnet.matic.network').content
            prices = json.loads(response)
            fast_price = float(prices[gas_reference_level])
            return min(fast_price + 0.25, max_gas_price)
        except Exception:
            return max_gas_price

    @staticmethod
    def _get_allowances(owned_tokens: List[TokenWithAmount], total_owned: float, wallet: Wallet) -> Dict[str, float]:
        """
            Calculate the total amount each token can still be swapped into, considering their risk factor
            and how much % they already represent. We add a 10% for not excluding tokens whenever
            the swapped amount is just above their maximum allowance
        """

        pending_to_amounts: Dict[str, float] = SwapperV3._get_pending_to_amounts(wallet)
        tokens_allowances: Dict[str, TokenWithAmount] = {}
        for token in owned_tokens:
            pending_in_tx = pending_to_amounts.get(token.token.symbol, 0)
            maximum_allowed = token.token.maximum_risk_allowed * total_owned - token.adjusted_amount - pending_in_tx
            maximum_allowed = max(maximum_allowed, 0)
            truncated = TokenWithAmount.truncate_adjusted_amount_at_second_digit(maximum_allowed)
            tokens_allowances[token.token.symbol] = truncated

        return tokens_allowances

    @staticmethod
    def top_up_gas(wallet: Wallet, web3: Web3):

        # Get token with highest amount and set its amount to 1$, so only that will be swapped
        owned_tokens: List[TokenWithAmount] = WalletChecker.get_tokens_amounts(wallet.address)
        most_owned_token: TokenWithAmount = sorted(owned_tokens, key=lambda tk: -tk.adjusted_amount)[0]
        most_owned_token.set_amount_from_adjusted(1)

        # Get non-swappable gas token
        gas_token = Token.even_non_swappable_objects.get(swappable=False)
        
        # Top up wallet
        gas_price = SwapperV3._get_gas_price()
        slippage = 0.5
        tx_address = ExchangeProxyV2.swap_for_gas_token(wallet, most_owned_token, gas_token, gas_price, slippage, web3)
        
        # store successful transaction (it will be checked via a cronjob)
        transaction = Transaction.objects.create(
            wallet=wallet,
            token_from=most_owned_token.token,
            token_to=gas_token,
            amount_from=most_owned_token.adjusted_amount,
            amount_to=0,
            address=tx_address,
            endpoint_uri=web3.provider.endpoint_uri[:40],
            gas_price=gas_price,
            slippage=slippage,
            type=Transaction.Type.TOPUP,
        )
