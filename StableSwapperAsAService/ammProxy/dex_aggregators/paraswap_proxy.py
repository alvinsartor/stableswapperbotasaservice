# Python
from typing import List, Dict, Any, Optional
import concurrent.futures
import traceback
import time
import json
from requests.exceptions import Timeout

# 3rd Party
import requests
from web3 import Web3

# Django
from django.conf import settings

# Local
from kernel.models import Token
from kernel.models import Wallet
from blockchainProxy.token_with_amount import TokenWithAmount
from blockchainProxy.quote_with_tx_data import QuoteWithTxData
from blockchainProxy.transactions_signer import TransactionsSigner
from ammProxy.dex_aggregators.base_exchange import BaseExchange


class ParaswapProxy(BaseExchange):
   
    DEX_NAME = "Paraswap"
    SUPPORTED_CHAINS = [137]
    
    def get_raw_transaction(
        self,
        wallet: Wallet,
        token_from: TokenWithAmount, 
        token_to: Token, 
        gas_price: float, 
        slippage: float, 
        nonce: int,
        w3: Web3,
    ) -> Optional[QuoteWithTxData]:
        print("Starting PARASWAP quote from %s to %s" % (token_from.token.symbol, token_to.symbol))
        start = time.time()
        
        # Retrieve quote
        quote = self._get_quote_data(wallet, token_from, token_to)

        # Get the amount         
        destinationAmount = float(quote["destAmount"])
        token_with_amount = TokenWithAmount(token_to, destinationAmount)

        print("Retrieved PARASWAP quote from %s to %s: %s. Now retrieving tx data" % (token_from.token.symbol, token_to.symbol, token_with_amount.adjusted_amount))

        # Get the tx data
        tx_data = self._get_tx_data(wallet, token_from, token_to, gas_price, slippage, nonce, w3, quote)

        # Wrap it up
        print("Ending quote from %s to %s (elapsed: %s)" % (token_from.token.symbol, token_to.symbol, time.time() - start))        
        return QuoteWithTxData(token_with_amount, slippage, tx_data)
    

 
    def _get_quote_data(
        self,
        wallet: Wallet,
        token_from: TokenWithAmount, 
        token_to: Token
    ) -> Dict[str, Any]:
        params = {
            'srcToken': token_from.token.address,
            'srcDecimals': token_from.token.decimals,
            'destToken': token_to.address,
            'destDecimals': token_from.token.decimals,
            'amount': f"{token_from.amount:.0f}",  # suppress scientific notation
            'network': settings.BLOCKCHAIN_ID,
            'userAddress': wallet.address,
        }

        url = 'https://apiv5.paraswap.io/prices'

        try:
            response = requests.get(url, params=params, timeout=10)
            response_json = response.json()
        except Timeout:
            raise Exception("%s->%s: Timeout" %(token_from.token.symbol, token_to.symbol))
        except Exception as exc:
            raise Exception("%s->%s: %s" %(token_from.token.symbol, token_to.symbol, response.content))        
        if response.status_code != 200:
            error_message = response_json.get('error', json.dumps(response_json))
            raise Exception("%s->%s: %s" %(token_from.token.symbol, token_to.symbol, error_message))

        return response_json['priceRoute']

    
    def _get_tx_data(
        self,
        wallet: Wallet,
        token_from: TokenWithAmount, 
        token_to: Token, 
        gas_price: float, 
        slippage: float, 
        nonce: int,
        w3: Web3,
        quote_data: Dict[str, Any]
    ) -> Optional[QuoteWithTxData]:
        gas_price_in_gwei = gas_price * pow(10, 9)
        headers = { 'Referer': 'StableSwapperBot', 'content-type': 'application/json'}
        body = {
            'priceRoute': quote_data,
            'srcToken': w3.toChecksumAddress(token_from.token.address),
            'srcDecimals': token_from.token.decimals,
            'destToken': w3.toChecksumAddress(token_to.address),
            'destDecimals': token_from.token.decimals,
            'srcAmount': f"{token_from.amount:.0f}",  # suppress scientific notation
            'slippage': slippage * 100,
            'userAddress': wallet.address,
        }
        
        url = 'https://apiv5.paraswap.io/transactions/%s?gasPrice=%s' % (settings.BLOCKCHAIN_ID, f"{gas_price_in_gwei:.0f}")

        try:
            response = requests.post(url, json=body, headers=headers, timeout=10)
            response_json = response.json()
        except Timeout:
            raise Exception("%s->%s: Timeout" %(token_from.token.symbol, token_to.symbol))
        except Exception as exc:
            raise Exception("%s->%s: %s" %(token_from.token.symbol, token_to.symbol, response.content))        
        if response.status_code != 200:
            error_message = response_json.get('error', json.dumps(response_json))
            
            # If it is an allowance issue, let's fix it right away
            if "Not enough" in error_message and "allowance" in error_message:
                self._give_allowance(wallet, token_from.token, quote_data['tokenTransferProxy'], nonce, w3)
                raise Exception("%s->%s: Not enough allowance. Giving it now..." %(token_from.token.symbol, token_to.symbol))

            raise Exception("%s->%s: %s" %(token_from.token.symbol, token_to.symbol, error_message))

        self._prepare_transaction_for_submission(response_json, nonce)
        return response_json

    def _prepare_transaction_for_submission(self, tx: Dict[str, Any], nonce):
        """ Final preparations so the tx data can be sent to the blockchain """
        tx.update(
            {
                'nonce': nonce,
                'value': int(tx['value']),
                'gasPrice': int(tx['gasPrice']),
                'gas': int(tx['gas']),
            }
        )