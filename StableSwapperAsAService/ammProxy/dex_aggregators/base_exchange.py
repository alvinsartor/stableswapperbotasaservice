
# Python
from abc import ABC, abstractmethod
from typing import List, Optional, Tuple
import time

# Django
from django.conf import settings

# 3rd Party
import requests
from web3 import Web3

# Local
from kernel.models import Token
from kernel.models import Wallet
from kernel.models import Transaction
from blockchainProxy.token_with_amount import TokenWithAmount
from blockchainProxy.quote_with_tx_data import QuoteWithTxData

tokenABI = [
    {
        "constant": "false",
        "inputs": [{"name": "_spender", "type": "address"}, {"name": "_value", "type": "uint256" }],
        "name": "approve",
        "outputs": [{"name": "", "type": "bool"}],
        "payable": "false",
        "stateMutability": "nonpayable",
        "type": "function"
    },
] 

class BaseExchange(ABC):
        
    def __init__(self):
        if settings.BLOCKCHAIN_ID not in self.SUPPORTED_CHAINS:
            raise Exception("The dex %s does not support the chain %s" % (self.DEX_NAME, settings.BLOCKCHAIN_ID))

    @property
    @abstractmethod
    def DEX_NAME(self) -> List[int]:
        """ The name this DEX likes to be called with """

    @property
    @abstractmethod
    def SUPPORTED_CHAINS(self) -> List[int]:
        """ The list of chains this proxy can operate on """

    @abstractmethod
    def get_raw_transaction(
        self,
        wallet: Wallet,
        token_from: TokenWithAmount, 
        token_to: Token, 
        gas_price: float, 
        slippage: float, 
        nonce: int,
        w3: Web3,
    ) -> Optional[QuoteWithTxData]:
        """ Gets a transaction object that can be submitted to the blockchain """

    def _give_allowance(
        self, 
        wallet: Wallet,
        token: Token,
        spender_address: str,
        nonce: int,
        w3: Web3
    ) -> None:
        print("Giving allowance for %s to contract %s" % (token.symbol, spender_address))        
        
        gas_price = 0
        tx_address = self._send_allowance_tx_to_the_blockchain(wallet, token, spender_address, nonce, w3)
        
        # store successful transaction (it will be checked via a cronjob)
        transaction = Transaction.objects.create(
            wallet=wallet,
            token_from=token,
            token_to=token,
            amount_from=0,
            amount_to=0,
            address=tx_address,
            endpoint_uri=w3.provider.endpoint_uri[:40],
            gas_price=gas_price,
            slippage=0,
            type=Transaction.Type.ALLOWANCE,
        )

    def _send_allowance_tx_to_the_blockchain(
        self, 
        wallet: Wallet,
        token: Token, 
        spender_address: str,
        nonce: int,
        w3: Web3,
    ) -> str:
        checksum_token_addr = Web3.toChecksumAddress(token.address)
        checksum_wallet_addr = Web3.toChecksumAddress(wallet.address)
        checksum_spender_address_addr = Web3.toChecksumAddress(spender_address)
        max_amount = Web3.toWei(2**64-1,'ether')

        contract = w3.eth.contract(address=checksum_token_addr, abi=tokenABI)
        tx = contract.functions.approve(checksum_spender_address_addr, max_amount).buildTransaction({
            'from': checksum_wallet_addr, 
            'nonce': nonce
            })
        signed_tx = w3.eth.account.sign_transaction(tx, wallet.private_key)
        tx_address = w3.eth.send_raw_transaction(signed_tx.rawTransaction)
        return tx_address.hex()
