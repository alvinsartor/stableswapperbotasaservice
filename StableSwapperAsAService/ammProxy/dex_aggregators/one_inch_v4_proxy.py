# Python
from typing import List, Dict, Any, Optional, Tuple
import time
import json
from requests.exceptions import Timeout

# 3rd Party
import requests
from web3 import Web3

# Django
from django.conf import settings

# Local
from kernel.models import Token
from kernel.models import Wallet
from blockchainProxy.token_with_amount import TokenWithAmount
from blockchainProxy.quote_with_tx_data import QuoteWithTxData
from blockchainProxy.transactions_signer import TransactionsSigner
from ammProxy.dex_aggregators.base_exchange import BaseExchange


class OneInchV4Proxy(BaseExchange):
   
    DEX_NAME = "1InchV4"
    SUPPORTED_CHAINS = [137]
    
    def get_raw_transaction(
        self,
        wallet: Wallet,
        token_from: TokenWithAmount, 
        token_to: Token, 
        gas_price: float, 
        slippage: float, 
        nonce: int,
        w3: Web3,
    ) -> Optional[QuoteWithTxData]:
        print("Starting 1INCH quote from %s to %s" % (token_from.token.symbol, token_to.symbol))
        start = time.time()

        gas_price_in_gwei = gas_price * pow(10, 9)
        params = {
            'fromTokenAddress': token_from.token.address,
            'toTokenAddress': token_to.address,
            'amount': f"{token_from.amount:.0f}",  # suppress scientific notation
            'slippage': slippage,
            'fromAddress': wallet.address,
            'allowPartialFill': True,
            'gasPrice': f"{gas_price_in_gwei:.0f}",
        }

        url = 'https://ananas.api.enterprise.1inch.exchange/v4.0/%s/swap' % settings.BLOCKCHAIN_ID

        try:
            response = requests.get(url, params=params, timeout=10)
            response_json = response.json()
        except Timeout:
            raise Exception("%s->%s: Timeout" %(token_from.token.symbol, token_to.symbol))
        except Exception as exc:
            raise Exception("%s->%s: %s" %(token_from.token.symbol, token_to.symbol, response.content))        
        if response.status_code != 200:
            error_message = response_json.get('message', json.dumps(response_json))

            # If it is an allowance issue, let's fix it right away
            if "Not enough allowance" in error_message:
                spender = error_message.split(' ')[-1]
                self._give_allowance(wallet, token_from.token, spender, nonce, w3)
                raise Exception("%s->%s: Not enough allowance. Giving it now..." %(token_from.token.symbol, token_to.symbol))

            raise Exception("%s->%s: %s" %(token_from.token.symbol, token_to.symbol, error_message))



            raise Exception("%s->%s: %s" %(token_from.token.symbol, token_to.symbol, error_message))
                            
        # prepare token with amount
        quote = float(response_json['toTokenAmount'])
        token_with_amount = TokenWithAmount(token_to, quote)

        # prepare quote with tx data
        tx_data = response_json['tx']
        self._prepare_transaction_for_submission(tx_data, nonce, w3)
        print("Ending quote from %s to %s (elapsed: %s)" % (token_from.token.symbol, token_to.symbol, time.time() - start))        
        return QuoteWithTxData(token_with_amount, slippage, tx_data)
    
    def _prepare_transaction_for_submission(self, tx: Dict[str, Any], nonce, w3: Web3):
        """ Final preparations so the tx data can be sent to the blockchain """
        tx.update(
            {
                'nonce': nonce,
                'to': w3.toChecksumAddress(tx['to']),
                'value': int(tx['value']),
                'gasPrice': int(tx['gasPrice']),
                'chainId': settings.BLOCKCHAIN_ID,
            }
        )
    