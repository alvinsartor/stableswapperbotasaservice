# Python
from typing import List, Dict, Optional, Tuple
import concurrent.futures
import time

# 3rd Party
from web3 import Web3

# Django
from django.conf import settings

# Local
from kernel.models import Token
from kernel.models import Wallet
from blockchainProxy.token_with_amount import TokenWithAmount
from blockchainProxy.quote_with_tx_data import QuoteWithTxData
from blockchainProxy.transactions_signer import TransactionsSigner
from ammProxy.dex_aggregators.base_exchange import BaseExchange
from ammProxy.dex_aggregators.one_inch_proxy import OneInchProxy
from ammProxy.dex_aggregators.one_inch_v4_proxy import OneInchV4Proxy
from ammProxy.dex_aggregators.paraswap_proxy import ParaswapProxy
from sharedSettings.models import SharedSettings


AGGREGATORS: Dict[str, BaseExchange] = {
    "1INCH": OneInchProxy(),
    "1INCHV4": OneInchProxy(),
    "PARASWAP": ParaswapProxy(),
}

class ExchangeProxyV2:

    @staticmethod
    def _get_aggregator() -> BaseExchange:
        exchange = SharedSettings.get('EXCHANGE')
        if exchange not in AGGREGATORS:
            raise Exception("Exchange '%s' is not supported" % exchange)    
        return AGGREGATORS[exchange]

    @staticmethod
    def get_best_swap_quote(
        wallet: Wallet,
        token_with_amount: TokenWithAmount, 
        swappable_tokens: List[Token], 
        gas_price: float, 
        slippage: float,
        web3: Web3,
    ) -> Tuple[QuoteWithTxData, str] :
        aggregator = ExchangeProxyV2._get_aggregator()

        print(" - - Starting quote for %s - - " % token_with_amount.token.symbol)
        start = time.time()

        nonce = web3.eth.get_transaction_count(wallet.address, 'pending')
        quotes: List[Optional[QuoteWithTxData]] = []
        errors: List[str] = []

        def _get_quote_and_append_result(token: Token) -> None:
            try:
                quote = aggregator.get_raw_transaction(wallet, token_with_amount, token, gas_price, slippage, nonce, web3)
                quotes.append(quote)
            except Exception as error:
                errors.append(str(error))

        with concurrent.futures.ThreadPoolExecutor(max_workers=15) as executor:
            executor.map(_get_quote_and_append_result, swappable_tokens)

        non_failed_quotes = [quote for quote in quotes if quote]
        print(" + + Ended quote for %s (elapsed: %s) + + " % (token_with_amount.token.symbol, time.time() - start))

        for qt in non_failed_quotes:
            print("Quote token %s, Amount: %s, Minimum granted: %s" % (qt.token_with_amount.token.symbol, qt.token_with_amount.adjusted_amount, qt.minimum_granted))

        check_errors = ", ".join(errors)
        if non_failed_quotes:
            best_quote = sorted(non_failed_quotes, key=lambda q: -q.minimum_granted)[0]
            return best_quote, check_errors        

        raise Exception('All quotes failed for %s. Errors: %s' % (token_with_amount.token.symbol, check_errors))

    @staticmethod
    def swap_for_gas_token(
        wallet: Wallet, 
        token_from: TokenWithAmount, 
        gas_token: Token, 
        gas_price: float, 
        slippage: float, 
        w3: Web3
    ) -> Optional[str]:
        nonce = w3.eth.get_transaction_count(wallet.address, 'pending')
        aggregator = ExchangeProxyV2._get_aggregator()
        quote = aggregator.get_raw_transaction(wallet, token_from, gas_token, gas_price, slippage, nonce, w3)
        return TransactionsSigner.sign_and_submit(quote, wallet, w3) if quote else None
