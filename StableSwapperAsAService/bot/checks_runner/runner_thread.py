# Python
import threading
import traceback
import time

# Django
from django.utils import timezone

# 3rd Party
from web3 import Web3

# Local
from bot.models import ChecksRunnerSession
from sharedSettings.models import SharedSettings
from ammProxy.swapper_v3 import SwapperV3
from blockchainProxy.wallet_checker import WalletChecker
from blockchainProxy.web3_factory import Web3Factory


class RunnerThread(threading.Thread):
    def __init__(self, session_id: int, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.session_id = session_id

    def run(self) -> None:
        try:
            session = ChecksRunnerSession.objects.get(id=self.session_id)
            iterations_before_shutdown = int(SharedSettings.get('CHECK_RUNNER_ITERATIONS'))
            print("Bot started for session %s" % self.session_id)
            web3 = Web3Factory.get_fast_w3_instance()
            self.top_up_if_low_in_gas(session, web3)
            
            while iterations_before_shutdown > 0:
                print("Bot with session %s is performing check %s" % (self.session_id, iterations_before_shutdown))
                session.refresh_from_db()
                if session.should_shutdown:
                    break

                SwapperV3.swap_if_possible(session.wallet, web3)

                session.refresh_from_db()
                session.last_check_date = timezone.now()
                session.check_batches_triggered += 1
                session.save()

                if session.check_batches_triggered % 100 == 0:
                    self.top_up_if_low_in_gas(session, web3)

                iterations_before_shutdown -= 1

                # Rate-Limited wallets are paused between each cicle
                if not session.should_shutdown and session.wallet.rate_limited > 0:
                    time.sleep(session.wallet.rate_limited)

        except Exception as error:
            session.error = "%s\n\nStacktrace:\n%s" % (str(error), traceback.format_exc())

        session.shutdown_date = timezone.now()
        session.ran_until_completion = iterations_before_shutdown == 0
        session.save()

    def top_up_if_low_in_gas(self, session: ChecksRunnerSession, web3: Web3) -> None:
        matic_balance = WalletChecker.get_matic_balance(session.wallet.address, web3).adjusted_amount
        if matic_balance < float(SharedSettings.get('MINIMUM_GAS_BEFORE_TOP_UP')):
            print("Topping up bot with session %s" % self.session_id)
            SwapperV3.top_up_gas(session.wallet, web3)