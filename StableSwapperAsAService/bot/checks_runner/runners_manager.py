# Python
from typing import Optional

# Django
from django.utils import timezone

# Local
from bot.models import ChecksRunnerSession
from bot.checks_runner.runner_thread import RunnerThread
from kernel.models import Wallet


class RunnersManager:
    @staticmethod
    def start_new_runner(wallet: Wallet):
        """ Starts a new runner (unless an existing one is already running) """

        if RunnersManager.is_runner_active(wallet):
            raise Exception("You're tring to start a runner while another one is already active.")

        # Create a new session that will be passed to the checks runner
        new_session = ChecksRunnerSession.objects.create(wallet=wallet)

        runner = RunnerThread(new_session.id)
        runner.setDaemon(True)
        runner.start()

    @staticmethod
    def ask_current_runner_to_shutdown(wallet: Wallet) -> None:
        """ Ask the runner to terminate at the next cycle and close the session when it does so """
        session = RunnersManager._get_current_session(wallet)
        if not session:
            return
        session.should_shutdown = True
        session.save()

    @staticmethod
    def close_sessions_for_runners_that_are_gone_awol(wallet: Wallet) -> None:
        """ if a runner has been asked to shutdown but is not responding, we just close their session """
        awol_runners = ChecksRunnerSession.objects.filter(wallet=wallet, shutdown_date__isnull=True, should_shutdown=True)
        awol_runners.update(shutdown_date=timezone.now())

    @staticmethod
    def is_runner_active(wallet: Wallet) -> bool:
        """ Checks whether a runner is already active """
        return RunnersManager._get_current_session(wallet) is not None

    @staticmethod
    def is_runner_idle(wallet: Wallet) -> bool:
        """ Checks whether a runner is active but no checks are being performed since a while """
        session = RunnersManager._get_current_session(wallet)
        return session.is_runner_idle if session else False

    @staticmethod
    def _get_current_session(wallet: Wallet) -> Optional[ChecksRunnerSession]:
        """ The session bound to the active runner """
        try:
            return ChecksRunnerSession.objects.get(wallet=wallet, shutdown_date__isnull=True)
        except ChecksRunnerSession.DoesNotExist:
            return None
