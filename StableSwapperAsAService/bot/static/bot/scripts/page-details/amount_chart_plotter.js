
async function plotAmountChart()
{
    const wallet_address = document.getElementById('id-wallet-address').value;
    const canvas = document.getElementById('amount-chart');
    const rawData = await $.getJSON('/statistics/data/amount_over_time/' + wallet_address + "/");
    
    Chart.defaults.plugins.legend.display = false;
    Chart.defaults.responsive = true;
    Chart.defaults.maintainAspectRatio = false;

    new Chart(canvas, {
        type: 'line',
        data: {
            labels: rawData.labels.reverse(),
            datasets: [{
                data: rawData.data.reverse(),
                fill: false,
                borderColor: 'rgb(4, 167, 119)',
                tension: 0.1
            }],
        },        
    });
} 

plotAmountChart();