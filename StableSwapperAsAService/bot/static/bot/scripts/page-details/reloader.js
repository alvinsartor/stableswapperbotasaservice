
const originalTitle = document.title;
const reloadFrequencyInSeconds = 60;
const updateFrequencyInSeconds = 1;

function updateCounter(remaining)
{
    document.title = originalTitle + " - " + remaining + "s";
    setTimeout(() => { updateCounter(remaining - updateFrequencyInSeconds); }, updateFrequencyInSeconds * 1000);
}

function reloadInFiveMinutes()
{
    setTimeout(() => { window.location.reload(); }, reloadFrequencyInSeconds * 1000);
    updateCounter(reloadFrequencyInSeconds)
}

reloadInFiveMinutes();