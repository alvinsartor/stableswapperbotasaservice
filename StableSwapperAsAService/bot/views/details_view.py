# Local
import math
from typing import Dict, Any
from datetime import timedelta

# Django
from django.utils import timezone
from django.views.generic.base import TemplateView
from django.shortcuts import get_object_or_404

# Local
from blockchainProxy.wallet_checker import WalletChecker
from kernel.models import Transaction
from kernel.models import Wallet
from bot.models import Check
from sharedSettings.models import SharedSettings
from statistics.helpers.profit_and_interests import ProfitAndInterests


class DetailsView(TemplateView):
    template_name = 'bot/page-details.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        
        wallet_address = self.kwargs.get('wallet_address', None)
        wallet = get_object_or_404(Wallet, address=wallet_address)
        
        context.update(DetailsView.get_statistics(wallet))

        context['wallet_address'] = wallet.address
        context['latest_transactions'] = Transaction.objects.filter(wallet=wallet).select_related('token_from', 'token_to').order_by('-date')[:20]
        context['nr_transactions'] = Transaction.objects.filter(wallet=wallet).count()

        context['latest_checks'] = Check.objects.filter(wallet=wallet).select_related('token_to_swap_from', 'token_to_swap_to').order_by('-date')[:19]
        context['nr_checks'] = Check.objects.filter(wallet=wallet).count()
        context['MINIMUM_SWAPPABLE'] = float(SharedSettings.get('MINIMUM_SWAPPABLE'))

        return context

    @staticmethod
    def get_statistics(wallet: Wallet) -> Dict[str, Any]:
        context = {}

        context['owned_tokens'] = sorted(
            WalletChecker.get_tokens_amounts(wallet.address), key=lambda tk: tk.adjusted_amount, reverse=True
        )
        context['owned_tokens'].insert(0, WalletChecker.get_matic_balance(wallet.address))

        last_week = timezone.now() - timedelta(days=7) 
        txs = Transaction.objects.filter(date__gte=last_week, wallet__address=wallet.address, state=Transaction.States.CONFIRMED)
        profits_and_interests = ProfitAndInterests(txs, wallet)

        context['total_owned'] = profits_and_interests.total_owned
        context['initial'] = profits_and_interests.initially_owned
        context['initial_date'] = profits_and_interests.initial_date
        context['profit_abs'] = profits_and_interests.total_profit
        context['profit_perc'] = profits_and_interests.profit_percentage
        context['daily_APR'] = profits_and_interests.daily_APR
        context['APR'] = profits_and_interests.APR
        context['APY'] = profits_and_interests.APY
        
        return context
