# Local
from datetime import timedelta

# Django
from django.utils import timezone
from django.http.response import JsonResponse
from django.views.generic.base import View

# Local
from blockchainProxy.transactions_checker import TransactionsChecker
from blockchainProxy.wallet_checker import WalletChecker
from blockchainProxy.web3_factory import Web3Factory
from kernel.models import Transaction


class ConfirmTransactionTriggerView(View):
    def get(self, request, *args, **kwargs):
        transactions_to_confirm = Transaction.get_transactions_to_confirm().order_by('-date')
        outcome = {'uncertain': 0, 'confirmed': 0, 'failed': 0, 'still_waiting': 0, 'not_found': 0}

        if not transactions_to_confirm:
            return JsonResponse(outcome)

        w3 = Web3Factory.get_fast_w3_instance()
        for transaction in transactions_to_confirm:
            if not transaction.address:
                # There is no address, we cannot check the transaction
                transaction.state = Transaction.States.UNCERTAIN
                transaction.save()
                outcome['uncertain'] += 1
                continue

            tx_receipt = TransactionsChecker.get_transaction_receipt(transaction.address, w3)
            ten_mins_ago = timezone.now() - timedelta(minutes=10)
            if not tx_receipt and transaction.date > ten_mins_ago:
                # The transaction was not found, but it's not that old, let's wait a bit longer
                outcome['still_waiting'] += 1
                continue

            if not tx_receipt and transaction.date <= ten_mins_ago:
                # The transaction was not found and it is old, let's consider it as failed
                transaction.state = Transaction.States.NOT_FOUND
                transaction.save()
                outcome['not_found'] += 1
                continue

            did_tx_go_through = TransactionsChecker.was_transaction_executed(tx_receipt)
            if did_tx_go_through:
                # The transaction was found and it went through! Hurray!
                transaction.state = Transaction.States.CONFIRMED
                transaction.gas_used = TransactionsChecker.get_transaction_gas_used(tx_receipt)
                transaction.block = TransactionsChecker.get_transaction_block(tx_receipt)
                transaction.execution_date = TransactionsChecker.get_transaction_time(tx_receipt, w3)

                if transaction.type == Transaction.Type.SWAP:
                    # If it is a swap, we calculate the profit by checking the tx logs
                    transaction.profit = TransactionsChecker.get_swap_profit(tx_receipt)
                elif transaction.type == Transaction.Type.ALLOWANCE:
                    # If it is an allowance request, there is no profit or loss
                    transaction.profit = 0
                else:
                    # Otherwise it is a TOPUP and we know that there was no profit, we simply paid 'amount_from'
                    transaction.profit = -transaction.amount_from

                transaction.save()
                outcome['confirmed'] += 1
                continue

            # The transaction was found but it didn't go through.. Shame.
            transaction.gas_used = TransactionsChecker.get_transaction_gas_used(tx_receipt)
            transaction.state = Transaction.States.FAILED
            transaction.save()
            outcome['failed'] += 1
            continue

        return JsonResponse(outcome)
