# Python
from typing import Dict

# Django
from django.http.response import JsonResponse
from django.views.generic.base import View

# Local
from bot.checks_runner import RunnersManager
from kernel.models import Wallet


class ManageChecksRunnerTriggerView(View):
    def get(self, request, *args, **kwargs):

        results: Dict[str, int] = {
            "Shutdown": 0,
            "Skipped": 0,
            "Started": 0,
        }

        for wallet in Wallet.objects.all():

            # We gently ask the runner to shutdown, but if they do not respond
            # we can safely assume they're dead. In that case we just kill their session.
            RunnersManager.close_sessions_for_runners_that_are_gone_awol(wallet)

            # If the runner is idle, shut it down and wait for the next iteration
            if RunnersManager.is_runner_idle(wallet):
                RunnersManager.ask_current_runner_to_shutdown(wallet)
                results["Shutdown"] += 1
                continue

            # If the runner is not idle and already running (healty), we just leave the situation as it is
            if RunnersManager.is_runner_active(wallet):
                results["Skipped"] += 1
                continue

            # If no runner is started, we start one
            RunnersManager.start_new_runner(wallet)
            results["Started"] += 1

        return JsonResponse(results)