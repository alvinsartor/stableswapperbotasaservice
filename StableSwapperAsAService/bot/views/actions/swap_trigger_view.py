# Django
from django.http.response import JsonResponse
from django.views.generic.base import View

# Local
from ammProxy.swapper_v3 import SwapperV3
from kernel.models import Wallet
from blockchainProxy.web3_factory import Web3Factory

class SwapTriggerView(View):
    def get(self, request, *args, **kwargs):
        web3 = Web3Factory.get_fast_w3_instance()
        wallet = Wallet.objects.first()
        outcome = SwapperV3.swap_if_possible(wallet, web3)

        return JsonResponse({'success': outcome})
