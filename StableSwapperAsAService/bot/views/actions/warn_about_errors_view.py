# Python
from datetime import timedelta

# Django 
from django.urls import reverse
from django.http.response import JsonResponse 
from django.views.generic.base import View
from django.core.mail import send_mail 
from django.conf import settings 
from django.utils import timezone
 
# Local
from bot.models import Check
from kernel.models import Wallet


class WarnAboutErrorsView(View): 
    def get(self, request, *args, **kwargs):         
        for wallet in Wallet.objects.all():
            last_checks = Check.objects.filter(wallet=wallet).order_by('-date')[:30]            
            if all([check.failed for check in last_checks]):
                mail_text = "Last 30 checks resulted in error"
                self.send_error_email(request, mail_text, wallet)
                return JsonResponse({'problems retrieved': 'All checks are failing'})
            
            if last_checks[0].date < timezone.now() - timedelta(seconds=300):
                mail_text = "No check has been ran in the last 5 minutes. See check %s." % str(last_checks[0])
                self.send_error_email(request, mail_text, wallet)
                return JsonResponse({'problems retrieved': 'Checks are not being ran'})

        return JsonResponse({'problems retrieved': False})

    def send_error_email(self, request, message: str, wallet: Wallet) -> None:
        link = request.build_absolute_uri(reverse('bots:details', kwargs={'wallet_address': wallet.address}))
        
        send_mail(
            "Errors detected in SSB '%s'" % settings.INSTANCE_NAME, 
            message + "\nMore info at: %s" % link,
            None,
            [settings.DEFAULT_ADMIN_EMAIL],
            fail_silently=False
        ) 
        