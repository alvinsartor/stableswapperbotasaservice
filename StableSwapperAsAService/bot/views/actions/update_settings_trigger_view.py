# Python
from typing import List

# Django
from django.http.response import JsonResponse
from django.views.generic.base import View
from django.conf import settings

# 3rd Party
import requests

# Local
from sharedSettings.models import SharedSettings, PrivateSettings


class UpdateSettingsTriggerView(View):
    def get(self, request, *args, **kwargs):
        updated_private_settings = self.retrieve_and_update_private_settings_from_hive()
        updated_public_settings = self.retrieve_and_update_public_settings_from_hive()

        return JsonResponse({
            'updated private settings': updated_private_settings,
            'updated public settings': updated_public_settings,
        })
 
    def retrieve_settings_from_hive(self, url: str) -> List[str]:
        # Retrieve instance settings from hive
        headers = {'SSB-SECURITY-KEY': settings.SSB_SECURITY_KEY}
        try:
            response = requests.get(url, headers=headers)
        except Exception:
            raise Exception('I could not retrieve the settings from the hive')

        if response.status_code != 200:
            raise Exception('I could not retrieve the settings from the hive')
        return response.json()


    def retrieve_and_update_private_settings_from_hive(self) -> List[str]:
        # Retrieve instance settings from hive
        url = "%s/secrets/instance-private-settings/" % settings.HIVE_URL
        response_json = self.retrieve_settings_from_hive(url)

        updated_fields: List[str] = []
        instance_settings = PrivateSettings.objects.all()
        for setting in instance_settings:
            if setting.name in response_json and setting.value != response_json[setting.name]:
                updated_fields.append(setting.name)
                setting.value = response_json[setting.name]

        PrivateSettings.objects.bulk_update(instance_settings, ['value'])
        return updated_fields

    def retrieve_and_update_public_settings_from_hive(self) -> List[str]:
        # Retrieve instance settings from hive
        url = "%s/secrets/instance-public-settings/" % settings.HIVE_URL
        response_json = self.retrieve_settings_from_hive(url)

        updated_fields: List[str] = []
        instance_settings = SharedSettings.objects.all()
        for setting in instance_settings:
            if setting.name in response_json and setting.value != response_json[setting.name]:
                updated_fields.append(setting.name)
                setting.value = response_json[setting.name]

        SharedSettings.objects.bulk_update(instance_settings, ['value'])
        return updated_fields