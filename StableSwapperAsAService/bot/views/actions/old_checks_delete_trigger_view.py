# Python
from typing import Dict, Any
from datetime import timedelta

# Django
from django.conf import settings
from django.utils import timezone
from django.http.response import JsonResponse
from django.views.generic.base import View

# Local
from bot.models import Check
from bot.models import ChecksRunnerSession
from kernel.models import Transaction
from statistics.models import WalletDailyStatistic


class OldChecksDeleteTriggerView(View):
    def get(self, request, *args, **kwargs):        
        # Deleting checks and transactions
        result = self.hard_delete() if settings.DB_KEEP_MINIMAL else self.soft_delete()

        # Deleting ChecksRunnerSession
        one_day_ago = timezone.now() - timedelta(days=1)
        sessions_to_delete = ChecksRunnerSession.objects.filter(start_date__lte=one_day_ago, shutdown_date__isnull=False)
        result['deleted_checks_runner_sessions'] = sessions_to_delete.count()
        sessions_to_delete.delete()

        # Deleting Statistics
        two_months_ago = timezone.now() - timedelta(days=60)
        stats_to_delete = WalletDailyStatistic.objects.filter(date__lte=two_months_ago)
        result['deleted_statistics'] = stats_to_delete.count()
        stats_to_delete.delete()

        return JsonResponse(result)

    def soft_delete(self) -> Dict[str, Any]:
        """ Maintains data relative to 1 DAY of checks and 1 WEEK of transactions """
        result = {'Mode': 'Soft deletion'}
        
        one_day_ago = timezone.now() - timedelta(days=1)
        checks_to_delete = Check.objects.filter(date__lte=one_day_ago)
        result['deleted_checks'] = checks_to_delete.count()
        checks_to_delete.delete()

        one_week_ago = timezone.now() - timedelta(days=7)
        transactions_to_delete = Transaction.objects.filter(date__lte=one_week_ago)
        result['deleted_transactions'] = transactions_to_delete.count()
        transactions_to_delete.delete()

        return result

    def hard_delete(self) -> Dict[str, Any]:
        """ Maintains data relative to 1 HOUR of checks and 1 DAY of transactions """
        
        result = {'Mode': 'Hard deletion'}

        one_hour_ago = timezone.now() - timedelta(seconds=3600)
        checks_to_delete = Check.objects.filter(date__lte=one_hour_ago)
        result['deleted_checks'] = checks_to_delete.count()
        checks_to_delete.delete()

        one_day_ago = timezone.now() - timedelta(days=1)
        transactions_to_delete = Transaction.objects.filter(date__lte=one_day_ago)
        result['deleted_transactions'] = transactions_to_delete.count()
        transactions_to_delete.delete()

        return result