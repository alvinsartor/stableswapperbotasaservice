
# Python
from typing import Dict, Any, Tuple, List, Optional
from datetime import timedelta
import json
import logging
import concurrent.futures

# Django
from django.conf import settings
from django.utils import timezone
from django.views.generic import TemplateView
from django.core import serializers
from django.forms.models import model_to_dict

# 3rd Party
import requests

# Local
from kernel.models import Wallet
from statistics.models import WalletDailyStatistic

logger = logging.getLogger(__name__)


class WalletWithInfo:
    def __init__(self, wallet_address: str, stats: Optional[WalletDailyStatistic]):
        self.wallet = wallet_address
        self.stats = stats

    def to_dict(self):
        return {"wallet" : self.wallet, "stats": model_to_dict(self.stats) if self.stats else None }

class HomeView(TemplateView):
    template_name = 'bot/page-home.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['instance_name'] = settings.INSTANCE_NAME
        context['wallets_with_stats'] = HomeView.get_wallets_with_statistics()
        context['total_earned_today'] = self._get_total_earned(context['wallets_with_stats'])
        context['total_owned'] = self._get_total_owned(context['wallets_with_stats'])
        return context

    @staticmethod
    def get_wallets_with_statistics() -> List[WalletWithInfo]:
        all_wallets = Wallet.objects.all().values_list('address', flat=True)
        yesterday = (timezone.now() - timedelta(days=1)).date()
        all_stats = WalletDailyStatistic.objects.filter(date__gte=yesterday).select_related('wallet')

        # Keep the latest statistics for each wallet
        stats_per_address: Dict[str, WalletDailyStatistic] = {}
        for stat in all_stats:
            if stat.wallet.address not in stats_per_address or stats_per_address[stat.wallet.address].date < stat.date:
                stats_per_address[stat.wallet.address] = stat

        # As there might be wallets without stats, compare with the full list and create the WalletWithInfo objects
        wallets_with_stats: Tuple[WalletWithInfo] = []
        for wallet_address in all_wallets:
            wallet_stats = stats_per_address.get(wallet_address, None)
            wallets_with_stats.append(WalletWithInfo(wallet_address, wallet_stats))

        return wallets_with_stats

    def _get_total_earned(self, all_wallets: Dict[str, Any]) -> float:
        return sum([wallet.stats.total_delta_profit_in_transactions if wallet.stats else 0 for wallet in all_wallets])
        
    def _get_total_owned(self, all_wallets: Dict[str, Any]) -> float:
        return sum([wallet.stats.total_owned if wallet.stats else 0 for wallet in all_wallets])       