
from .home_view import HomeView as HomeView
from .details_view import DetailsView as DetailsView
from .checks_view import ChecksView as ChecksView
from .transactions_view import TransactionsView as TransactionsView
