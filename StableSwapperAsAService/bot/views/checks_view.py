# Django
from django.views.generic import ListView

# Local
from bot.models import Check


class ChecksView(ListView):
    template_name = 'bot/page-checks.html'
    model = Check
    context_object_name = 'checks'
    queryset = Check.objects.all().select_related('token_to_swap_from', 'token_to_swap_to').order_by('-date')
    paginate_by = 120

    def get_queryset(self):        
        wallet_address = self.kwargs.get('wallet_address', None)
        return super().get_queryset().filter(wallet__address=wallet_address)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['wallet_address'] = self.kwargs.get('wallet_address', None)
        return context