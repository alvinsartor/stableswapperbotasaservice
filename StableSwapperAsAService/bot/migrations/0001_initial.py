# Generated by Django 3.2.5 on 2021-08-13 19:03

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('kernel', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='ChecksRunnerSession',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('start_date', models.DateTimeField(auto_now_add=True)),
                ('shutdown_date', models.DateTimeField(blank=True, default=None, null=True)),
                ('last_check_date', models.DateTimeField(blank=True, default=None, null=True)),
                ('check_batches_triggered', models.IntegerField(default=0)),
                ('should_shutdown', models.BooleanField(default=False)),
                ('error', models.TextField(blank=True)),
                ('wallet', models.ForeignKey(editable=False, on_delete=django.db.models.deletion.CASCADE, to='kernel.wallet')),
            ],
        ),
        migrations.CreateModel(
            name='Check',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('date', models.DateTimeField(auto_now_add=True)),
                ('duration_seconds', models.FloatField(blank=True, null=True)),
                ('total_owned', models.FloatField(blank=True, null=True)),
                ('amount_to_swap_from', models.FloatField(blank=True, null=True)),
                ('amount_to_swap_to', models.FloatField(blank=True, null=True)),
                ('forecasted_profit', models.FloatField(blank=True, null=True)),
                ('aborted', models.BooleanField(blank=True, null=True)),
                ('error', models.TextField(blank=True, null=True)),
                ('raw_data', models.TextField(blank=True, default='')),
                ('token_to_swap_from', models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, related_name='check_from', to='kernel.token')),
                ('token_to_swap_to', models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, related_name='check_to', to='kernel.token')),
                ('transaction', models.OneToOneField(null=True, on_delete=django.db.models.deletion.CASCADE, related_name='bot_check', to='kernel.transaction')),
                ('wallet', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='kernel.wallet')),
            ],
        ),
    ]
