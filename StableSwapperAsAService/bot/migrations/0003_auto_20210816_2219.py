# Generated by Django 3.2.5 on 2021-08-16 20:19

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('bot', '0002_check_is_successful'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='check',
            name='is_successful',
        ),
        migrations.AddField(
            model_name='check',
            name='failed',
            field=models.BooleanField(default=False),
        ),
        migrations.AlterField(
            model_name='check',
            name='error',
            field=models.TextField(blank=True, default='', null=True),
        ),
    ]
