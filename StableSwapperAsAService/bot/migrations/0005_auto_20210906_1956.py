# Generated by Django 3.2.7 on 2021-09-06 17:56

from django.db import migrations, models
import django.utils.timezone


class Migration(migrations.Migration):

    dependencies = [
        ('bot', '0004_alter_check_transaction'),
    ]

    operations = [
        migrations.AlterField(
            model_name='check',
            name='date',
            field=models.DateTimeField(default=django.utils.timezone.now),
        ),
        migrations.AlterField(
            model_name='checksrunnersession',
            name='start_date',
            field=models.DateTimeField(default=django.utils.timezone.now),
        ),
    ]
