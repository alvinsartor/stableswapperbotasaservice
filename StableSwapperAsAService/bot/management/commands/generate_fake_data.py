# Python
from unittest import mock
from datetime import timedelta

# Django
from django.core.management.base import BaseCommand, CommandError
from django.conf import settings
from django.utils import timezone

# 3rd Party
import pytz
import factory

# Local
from accounts.models import User
from kernel.models import Wallet
from kernel.models import Token
from kernel.models import Transaction
from kernel.factories import TokenFactory
from bot.factories import CheckFactory
from bot.factories import ChecksRunnerSessionFactory
from statistics.models import WalletDailyStatistic
from statistics.factories import WalletDailyStatisticFactory
from statistics.models import JournalEntry
from statistics.factories import JournalEntryFactory

#
# NOTE: To run management commands from the shell:
#
# from django.core import management; management.call_command('generate_fake_data')
#

class Command(BaseCommand):
    help = 'Resets the DB and generates fake data'

    def handle(self, *args, **options):
        if not settings.DEBUG:
            raise Exception("You cannot generate fake data in production!")
        
        self.stdout.write('Deleting existing data..')
        User.objects.all().delete()
        Wallet.objects.all().delete()
        Transaction.objects.all().delete()
        Token.even_non_swappable_objects.all().delete()
        WalletDailyStatistic.objects.all().delete()
        JournalEntry.objects.all().delete()

        self.stdout.write('Creating superuser..')
        email = settings.DEFAULT_ADMIN_EMAIL
        password = settings.DEFAULT_ADMIN_PASSWORD
        user = User(username=email, email=email, is_superuser=True, is_staff=True)
        user.set_password(password)
        user.save()

        self.stdout.write('Creating test wallets..')
        wallet_1 = Wallet.objects.create(address=settings.TEST_WALLET_ADDRESS_1, private_key=settings.TEST_WALLET_PRIVATE_KEY_1)
        wallet_2 = Wallet.objects.create(address=settings.TEST_WALLET_ADDRESS_2, private_key=settings.TEST_WALLET_PRIVATE_KEY_2)

        self.stdout.write('Creating random tokens..')
        TokenFactory.add_all_tokens()

        self.stdout.write('Creating random checks and transactions..')
        CheckFactory.create_batch(750, wallet=wallet_1)
        CheckFactory.create_batch(750, wallet=wallet_2)
        
        self.stdout.write('Creating runner sessions..')
        ChecksRunnerSessionFactory.create_batch(100, wallet=wallet_1)
        ChecksRunnerSessionFactory.create_batch(100, wallet=wallet_2)
        
        self.stdout.write('Creating random statistics..')
        today = timezone.now().date()
        WalletDailyStatisticFactory.create_batch(2, date=today, wallet=factory.Iterator([wallet_1, wallet_2]))        
        WalletDailyStatisticFactory.create_batch(2, date=today - timedelta(days=1), wallet=factory.Iterator([wallet_1, wallet_2]))        
        WalletDailyStatisticFactory.create_batch(2, date=today - timedelta(days=2), wallet=factory.Iterator([wallet_1, wallet_2]))        
        
        self.stdout.write('Creating journal entries..')
        JournalEntryFactory.create_batch(30)

        self.stdout.write('Successfully created fake data')