# Django
from django.utils import timezone
from django.core.management.base import BaseCommand

# Local
from bot.models import ChecksRunnerSession


class Command(BaseCommand):
    help = 'Closes all active CheckRunner sessions (to be called whenever the project is deployed)'

    def handle(self, *args, **options):
        (
            ChecksRunnerSession.objects.filter(shutdown_date__isnull=True).update(
                should_shutdown=True, shutdown_date=timezone.now()
            )
        )
        self.stdout.write(self.style.SUCCESS('Successfully closed all ChecksRunner sessions'))
