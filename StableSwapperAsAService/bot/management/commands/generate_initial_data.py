# Django
from django.core.management.base import BaseCommand
from django.conf import settings

# Local
from accounts.models import User
from kernel.models import Token
from kernel.factories import TokenFactory

#
# NOTE: To run management commands from the shell:
#
# from django.core import management; management.call_command('generate_initial_data')
#

class Command(BaseCommand):
    help = 'Generates the initial data'

    def handle(self, *args, **options):
        if Token.objects.all().count() > 0:
            raise Exception("It seems that the data has already been generated")

        self.stdout.write('Creating superuser..')
        email = settings.DEFAULT_ADMIN_EMAIL
        password = settings.DEFAULT_ADMIN_PASSWORD
        user = User(username=email, email=email, is_superuser=True, is_staff=True)
        user.set_password(password)
        user.save()

        self.stdout.write('Creating tokens..')
        TokenFactory.add_all_tokens()
        
        self.stdout.write('Successfully generated initial data')