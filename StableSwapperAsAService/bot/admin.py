# Django
from django.contrib import admin

# Local
from bot.models import Check
from bot.models import ChecksRunnerSession


@admin.register(Check)
class CheckAdmin(admin.ModelAdmin):
    list_display = (
        'date',
        'token_to_swap_from',
        'token_to_swap_to',
        'forecasted_profit',
        'aborted',
        'has_error',
        'duration_seconds',
    )

    def has_error(self, check):
        return bool(check.error)

    
@admin.register(ChecksRunnerSession)
class ChecksRunnerSessionAdmin(admin.ModelAdmin):
    list_display = ('start_date', 'shutdown_date', 'check_batches_triggered', 'should_shutdown')
