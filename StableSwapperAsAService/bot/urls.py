
# Django
from django.urls import path

# Local
from bot import views
from bot.views import actions as action_views

app_name = 'bots'

# fmt: off
urlpatterns = [
    # Website
    path('', views.HomeView.as_view(), name='home'),
    path('details/<str:wallet_address>/', views.DetailsView.as_view(), name="details"),
    path('checks/<str:wallet_address>/', views.ChecksView.as_view(), name='checks-list'),
    path('transactions/<str:wallet_address>/', views.TransactionsView.as_view(), name='transactions-list'),

    # Actions
    path('action/trigger-swap/', action_views.SwapTriggerView.as_view()),
    path('action/manage-checks-runner/', action_views.ManageChecksRunnerTriggerView.as_view()),
    path('action/delete-old-checks/', action_views.OldChecksDeleteTriggerView.as_view()),
    path('action/confirm-transactions/', action_views.ConfirmTransactionTriggerView.as_view()),
    path('action/warn-about-errors/', action_views.WarnAboutErrorsView.as_view()),
    path('action/update-settings/', action_views.UpdateSettingsTriggerView.as_view()),
] 
# fmt: on
