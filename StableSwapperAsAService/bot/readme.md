# Bot

This app is where the action lies and where multiple components of the project are used together in order to generate profit.

The most important components of the bots are **Checks** and **Runners**.

## Checks

A Check represents the best quote at a given moment for a specific stablecoin. Whenever a check expected profit is higher than a specified threshold, a transaction is triggered.

Checks are executed sequentially and continuously.

## Runner

A Runner is an daemon that continuously executes checks. Each Runner manages a single wallet and uses a `CheckRunnerSession` to keep track of how it is performing.

Runners are killed if idle and have a maximum number of checks they will be runned for, before of terminating themselves.