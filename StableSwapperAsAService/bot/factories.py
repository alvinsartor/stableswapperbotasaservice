# Python
import random
from datetime import timedelta

# 3rd Party
import factory
from factory import fuzzy
from django.utils import timezone

# Local
from kernel.models import Token
from kernel.factories import TransactionFactory
from bot.models import Check, ChecksRunnerSession

POSSIBLE_CHECK_ERRORS = ['Error. Consciousness developed. Killing it.', 'Singularity intercepted', 'Some matter suddenly disappeared', 'Quantum calculation error']
POSSIBLE_SESSION_ERRORS = ['Error while counting till ∞', 'Whoops, I forgot what I was doing', '2+2=5?', 'Tried to divide by 0']


class CheckFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Check

    wallet = None
    date = factory.fuzzy.FuzzyDateTime(timezone.now() - timedelta(days=1), timezone.now())
    token_to_swap_from = factory.LazyAttribute(lambda x: Token.objects.all().order_by('?')[0])
    token_to_swap_to = factory.LazyAttribute(
        lambda x: Token.objects.exclude(symbol=x.token_to_swap_from.symbol).order_by('?')[0]
    )
    amount_to_swap_from = fuzzy.FuzzyFloat(100, high=150)
    amount_to_swap_to = factory.LazyAttribute(lambda x: x.amount_to_swap_from + random.uniform(0.1, 0.25))
    forecasted_profit = factory.LazyAttribute(lambda x: x.amount_to_swap_to - x.amount_to_swap_from)
    aborted = fuzzy.FuzzyChoice([True, True, True, True, True, False])
    transaction = factory.LazyAttribute(
        lambda x: None
        if x.aborted
        else TransactionFactory(
            wallet=x.wallet,
            token_from=x.token_to_swap_from,
            token_to=x.token_to_swap_to,
            amount_from=x.amount_to_swap_from,
            amount_to=x.amount_to_swap_to,
        )
    )
    error = factory.LazyAttribute(lambda x: None if random.uniform(0, 1) > 0.1 else random.choice(POSSIBLE_CHECK_ERRORS))
    duration_seconds = fuzzy.FuzzyFloat(2, high=5.5)
    total_owned = fuzzy.FuzzyFloat(450, high=700)
    

class ChecksRunnerSessionFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = ChecksRunnerSession

    wallet = None
    start_date = factory.fuzzy.FuzzyDateTime(timezone.now() - timedelta(days=1), timezone.now())
    shutdown_date = factory.LazyAttribute(lambda x: x.start_date + timedelta(seconds=300))
    last_check_date = factory.LazyAttribute(lambda x: x.start_date + timedelta(seconds=300))
    check_batches_triggered = fuzzy.FuzzyInteger(0, high=250)
    should_shutdown = factory.LazyAttribute(lambda x: random.uniform(0, 1) < 0.05)
    error = factory.LazyAttribute(lambda x: '' if random.uniform(0, 1) > 0.1 else random.choice(POSSIBLE_SESSION_ERRORS))
