# Django
from django.db import models
from django.utils import timezone

# Local
from kernel.models import Token
from kernel.models import Transaction
from kernel.models import Wallet


class Check(models.Model):
    date = models.DateTimeField(default=timezone.now)
    wallet = models.ForeignKey(Wallet, on_delete=models.CASCADE)
    duration_seconds = models.FloatField(blank=True, null=True)
    total_owned = models.FloatField(blank=True, null=True)
    token_to_swap_from = models.ForeignKey(Token, on_delete=models.CASCADE, related_name='check_from', null=True)
    amount_to_swap_from = models.FloatField(blank=True, null=True)
    token_to_swap_to = models.ForeignKey(Token, on_delete=models.CASCADE, related_name='check_to', null=True)
    amount_to_swap_to = models.FloatField(blank=True, null=True)
    forecasted_profit = models.FloatField(blank=True, null=True)
    aborted = models.BooleanField(blank=True, null=True)
    transaction = models.OneToOneField(Transaction, on_delete=models.CASCADE, null=True, blank=True, related_name="bot_check")
    raw_data = models.TextField(blank=True, default='')
    
    error = models.TextField(blank=True, null=True, default='')
    failed = models.BooleanField(default=False)

    def __str__(self):
        return "[id: %s, date: %s, aborted: %s]" % (self.id, self.date, self.aborted)