# Python
from typing import Dict, Any

# Django
from django.utils import timezone

# Local
from blockchainProxy.token_with_amount import TokenWithAmount


class QuoteWithTxData:
    def __init__(self, token_with_amount: TokenWithAmount, slippage: float, tx_data: Dict[str, Any]):
        self.token_with_amount = token_with_amount
        self.slippage = slippage
        self.tx_data = tx_data
        self.date = timezone.now()

    @property
    def minimum_granted(self) -> float:
        return self.token_with_amount.adjusted_amount * (1 - self.slippage / 100)
