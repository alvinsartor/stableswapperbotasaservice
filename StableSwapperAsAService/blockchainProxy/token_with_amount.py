# Python
import math
import decimal

# Local
from kernel.models import Token


class TokenWithAmount:
    def __init__(self, token: Token, amount: float):
        self.token = token
        self.amount = TokenWithAmount.truncate_at_second_digit(token, amount)

    @property
    def adjusted_amount(self):
        return self.amount / math.pow(10, self.token.decimals)

    def set_amount_from_adjusted(self, adjusted_amount):
        """ Modifies the amount of this instance to match the new adjusted amount """
        self.amount = adjusted_amount * math.pow(10, self.token.decimals)
        
    def with_adjusted_amount(self, adjusted_amount):
        """ Returns a new instance with a modified amount """
        new_amount = adjusted_amount * math.pow(10, self.token.decimals)
        return TokenWithAmount(self.token, new_amount)

    def __str__(self):
        return "%s - %s" % (self.token.symbol, self.adjusted_amount)

    @staticmethod
    def truncate_at_second_digit(token: Token, amount: float) -> float:
        # we don't really care about the infinitesimal small parts of a token,
        # but they might make the transaction fail due to rounding errors.
        # So let's just truncate them
        adjusted = amount / math.pow(10, token.decimals)
        truncated_at_second_digit = decimal.Decimal(adjusted).quantize(
            decimal.Decimal('.01'), rounding=decimal.ROUND_DOWN
        )
        restored = float(truncated_at_second_digit) * math.pow(10, token.decimals)
        return restored

    @staticmethod
    def truncate_adjusted_amount_at_second_digit(amount: float) -> float:
        return float(decimal.Decimal(amount).quantize(decimal.Decimal('.01'), rounding=decimal.ROUND_DOWN))
