# Python
import random

# Django
from django.conf import settings

# 3rd Party
from web3 import Web3
from web3.middleware import geth_poa_middleware
from blockchainProxy.web3_test_mock.web3_test_mock import Web3TestMock 
from sharedSettings.models import PrivateSettings


class Web3Factory:

    @staticmethod
    def _verify_node_and_inject_middleware(w3: Web3) -> Web3:
        try:
            passed = w3.isConnected()
        except:
            passed = False
                
        if not passed:
            raise Exception('Could not connect to node client at %s' % w3.provider.endpoint_uri)
    
        # This line is necessary to be able to use w3.eth.get_block(block_number)
        # No idea why!
        w3.middleware_onion.inject(geth_poa_middleware, layer=0)
        return w3

    @staticmethod
    def _get_w3_instance_from_rpc_url(url: str) -> Web3:
        w3 = Web3(Web3.HTTPProvider(url)) if url.startswith("http") else Web3(Web3.WebsocketProvider(url))
        return Web3Factory._verify_node_and_inject_middleware(w3)

    @staticmethod
    def get_fast_w3_instance() -> Web3:
        if settings.TEST:
            return Web3TestMock()

        rpc_urls = PrivateSettings.get('RPC_URLS')
        random_rpc_url = random.choice([url.strip() for url in rpc_urls.split(',')])
        try:
            # Get a random RPC url and try to connect with it. If it does not work, retry with another one.
            return Web3Factory._get_w3_instance_from_rpc_url(random_rpc_url)
        except:
            print("- - ERROR WHILE TRYING TO CONNECT TO RPC URL %s. RETRYING WITH OTHER URL - -" % random_rpc_url)            
            return Web3Factory.get_fast_w3_instance()

