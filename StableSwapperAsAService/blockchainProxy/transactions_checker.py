# Python
from datetime import datetime
from typing import Dict, Any, Optional, List
from hexbytes import HexBytes

# Django
from django.utils.timezone import make_aware

# 3rd Party
import pytz
from web3 import Web3
from web3.exceptions import TransactionNotFound

# Local
from kernel.models import Token
from blockchainProxy.token_with_amount import TokenWithAmount


class TransactionsChecker:
    @staticmethod
    def get_transaction_receipt(hex: str, w3: Web3) -> Optional[Dict[str, Any]]:
        try:
            return w3.eth.get_transaction_receipt(hex)
        except TransactionNotFound:
            return None

    @staticmethod
    def was_transaction_executed(tx_receipt: Dict[str, Any]) -> bool:
        return tx_receipt['status'] == 1

    @staticmethod
    def get_transaction_block(tx_receipt: Dict[str, Any]) -> int:
        return tx_receipt['blockNumber']

    @staticmethod
    def get_transaction_gas_used(tx_receipt: Dict[str, Any]) -> int:
        return tx_receipt['gasUsed']

    @staticmethod
    def get_transaction_time(tx_receipt: Dict[str, Any], w3: Web3) -> datetime:
        block_number = TransactionsChecker.get_transaction_block(tx_receipt)
        unix_timestamp = w3.eth.getBlock(block_number).timestamp
        tx_datetime = datetime.fromtimestamp(unix_timestamp)
        return make_aware(tx_datetime, timezone=pytz.timezone("UTC"))
    
    @staticmethod
    def get_swap_profit(tx_receipt: Dict[str, Any]) -> float:
        tx_logs = tx_receipt['logs']
        tx_wallet = tx_receipt['from'][2:].lower() # address without 0x       
        considered_tokens: Dict[str, Token] = {tk.address.lower(): tk for tk in Token.even_non_swappable_objects.all()}

        # The general idea is checking the logs and see whenever a transfer function is called
        # Whenever this is done we can spot its parts:

        #   {
        #      "address": "0xE840B73E5287865EEc17d250bFb1536704B43B21",
        #      "topics": [
        #          "0xddf252ad1be2c89b69c2b068fc378daa952ba7f163c4a11628f55a4df523b3ef",
        #          "0x000000000000000000000000ba57c17b326f8887cf0c9ddd848a3040a97c7738",
        #          "0x00000000000000000000000011431a89893025d2a48dca4eddc396f8c8117187",
        #      ],
        #      "data": "0x0000000000000000000000000000000000000000000000056bc75e2d63100000",
        #      "blockNumber": 17876096,
        #      "transactionHash": "0x69672459df6302e2b4a307fb8cee144d5ac679026ca0063b09b57471f755a3df",
        #      "transactionIndex": 20,
        #      "blockHash": "0x103557bfe8096810131876459614b549387c4533a427b793985c86e5e8410254",
        #      "logIndex": 244,
        #      "removed": False,
        #   }

        # check the topics! ERC20 tokens have a Transfer(from, to) function
        #      0xddf252ad1be2c89b69c2b068fc378daa952ba7f163c4a11628f55a4df523b3ef is the signature in HEX
        #      0x000000000000000000000000ba57c17b326f8887cf0c9ddd848a3040a97c7738 is the 'from' (left-padded with 0s)
        #      0x00000000000000000000000011431a89893025d2a48dca4eddc396f8c8117187 is the 'to' (left-padded with 0s)
        # 'address' contains the token address
        # 'data' contains the amount in HEX (in this case 100000000000000000000. That is 100 as mUSD has 18 digits)

        def get_hex_value(topic) -> str:
            return topic if isinstance(topic, str) else topic.hex()


        def is_token_transfer(topics: List[str]) -> bool:
            if len(topics) < 3:
                return False
            hexvalue = get_hex_value(topics[0])
            return hexvalue == '0xddf252ad1be2c89b69c2b068fc378daa952ba7f163c4a11628f55a4df523b3ef'

        # One more thing! Sometimes the receipt keys need to be stripped (i.e. " topics") *facepalm*
        tx_logs = [{k.strip(): v for (k, v) in line.items()} for line in tx_logs]

        # OK! Now you know the theory. Let's get all the exiting tokens (should be 1)
        lines = [log for log in tx_logs if is_token_transfer(log['topics']) and get_hex_value(log['topics'][1]).endswith(tx_wallet)]
        amounts = [TransactionsChecker._extract_sent_amount(line, considered_tokens) for line in lines]
        sent_amount = sum(amounts)

        # And now all the token transfers towards the address
        lines = [log for log in tx_logs if is_token_transfer(log['topics']) and get_hex_value(log['topics'][2]).endswith(tx_wallet)]
        amounts = [TransactionsChecker._extract_sent_amount(line, considered_tokens) for line in lines]
        received_amount = sum(amounts)

        return received_amount - sent_amount

    @staticmethod
    def _extract_sent_amount(line: Dict[str, Any], considered_tokens: Dict[str, Token]) -> float:       
        token = considered_tokens[line['address'].lower()]
        decimal_amount = float(int(line['data'], 16))
        token_with_amount = TokenWithAmount(token, decimal_amount)
        return token_with_amount.adjusted_amount
