# 3rd Party
from web3 import Web3

# Django
from django.conf import settings

# Local
from kernel.models import Wallet
from blockchainProxy.quote_with_tx_data import QuoteWithTxData


class TransactionsSigner:
    
    @staticmethod
    def sign_and_submit(quote: QuoteWithTxData, wallet: Wallet, w3: Web3) -> str:
        if settings.DEBUG:
            return "0x00"

        # sign and send transaction
        signed_transaction = w3.eth.account.sign_transaction(quote.tx_data, wallet.private_key)
        tx_address = w3.eth.send_raw_transaction(signed_transaction.rawTransaction)
        return tx_address.hex()
