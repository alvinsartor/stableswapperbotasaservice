# Blockchain Proxy

This app is used to abstract queries to the blockchain.
For example, to check the balance of a wallet or to extract data from a specific transaction in the blockchain.