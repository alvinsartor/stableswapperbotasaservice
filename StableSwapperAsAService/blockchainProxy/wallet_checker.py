# Python
import concurrent.futures
from functools import lru_cache
from typing import List, Optional
import time

# Django
from django.conf import settings

# 3rd Party
import requests
from web3 import Web3

# Local
from kernel.models import Token
from kernel.models import Wallet
from blockchainProxy.token_with_amount import TokenWithAmount
from blockchainProxy.web3_factory import Web3Factory
from sharedSettings.models import PrivateSettings


class WalletChecker:
    
    @staticmethod
    def get_tokens_amounts(wallet_address: str) -> List[TokenWithAmount]:                
        ttl_hash_used_for_caching_in_10_seconds_batches = round(time.time() / 10)
        return WalletChecker._get_cached_tokens_amount(wallet_address, ttl_hash_used_for_caching_in_10_seconds_batches)

    @staticmethod
    def get_total_owned(wallet_address: str) -> float:
        owned_tokens = WalletChecker.get_tokens_amounts(wallet_address)
        return sum([token.adjusted_amount for token in owned_tokens])

    @staticmethod
    @lru_cache(8)
    def _get_cached_tokens_amount(wallet_address: str, ttl_hash: int):
        del ttl_hash # only used for caching
        return (
            WalletChecker._get_tokens_amounts_via_covalenth(wallet_address) 
            or WalletChecker._get_tokens_amounts_via_unmarshal(wallet_address) 
            or WalletChecker._get_tokens_amounts_via_moralis(wallet_address)
        )

    @staticmethod
    def get_matic_balance(wallet_address: str, w3: Optional[Web3] = None) -> TokenWithAmount:
        url = "https://deep-index.moralis.io/api/v2/%s/balance?chain=polygon" % wallet_address 
        headers = {'content-type': 'application/json', 'X-API-Key': PrivateSettings.get('MORALIS_API_KEY') } 
        
        try:
            response = requests.get(url, headers=headers, timeout=5) 
        except requests.Timeout:
            raise Exception('Error during call at Moralis API')

        if response.status_code != 200: 
            raise Exception('Error during call at Moralis API')
 
        matic_amount = float(response.json()['balance']) 
        MATIC = Token.even_non_swappable_objects.get(swappable=False)
        return TokenWithAmount(MATIC, matic_amount)

    @staticmethod
    def _get_tokens_amounts_via_covalenth(wallet_address: str) -> List[TokenWithAmount]:                
        url = "https://api.covalenthq.com/v1/137/address/%s/balances_v2/?key=%s" % (wallet_address, PrivateSettings.get('COVALENT_API_KEY')) 
        
        try:
            response = requests.get(url, timeout=5)
        except requests.Timeout:
            return []
 
        if response.status_code != 200: 
            return []

        balances = {token['contract_address']:float(token['balance']) for token in response.json()['data']['items']} 
        result = [TokenWithAmount(token, balances.get(token.address, 0)) for token in Token.objects.filter(disabled=False)]                 
        return result 

    @staticmethod
    def _get_tokens_amounts_via_moralis(wallet_address: str) -> List[TokenWithAmount]:                
        url = "https://deep-index.moralis.io/api/v2/%s/erc20?chain=polygon" % wallet_address 
        headers = {'content-type': 'application/json', 'X-API-Key': PrivateSettings.get('MORALIS_API_KEY') } 
 
        try: 
            response = requests.get(url, headers=headers, timeout=5)
        except requests.Timeout:
            return []

        if response.status_code != 200: 
            return [] 
 
        balances = {token['token_address']:float(token['balance']) for token in response.json()} 
        result = [TokenWithAmount(token, balances.get(token.address, 0)) for token in Token.objects.filter(disabled=False)]                 
        return result 
    
    @staticmethod
    def _get_tokens_amounts_via_unmarshal(wallet_address: str) -> List[TokenWithAmount]:                
        url = "https://stg-api.unmarshal.io/v1/matic/address/%s/assets" % wallet_address 
        headers = {'content-type': 'application/json' } 
 
        try: 
            response = requests.get(url, headers=headers, timeout=5)
        except requests.Timeout:
            return []

        if response.status_code != 200: 
            return [] 
 
        balances = {token['contract_address']:float(token['balance']) for token in response.json()} 
        result = [TokenWithAmount(token, balances.get(token.address, 0)) for token in Token.objects.filter(disabled=False)]                 
        return result 
