
# Django
from django.test import TestCase

# Local
from blockchainProxy.token_with_amount import TokenWithAmount
from kernel.factories import TokenFactory


class TokenWithAmountTest(TestCase):

    def test_amount_is_adjusted_to_token_decimals(self) -> None:
        raw_value = 123000000
        token = TokenFactory(symbol='CAT', decimals=6)
        token_with_amount = TokenWithAmount(token, raw_value)

        self.assertEqual(token_with_amount.adjusted_amount, 123.0)
        
    def test_amount_is_truncated_at_the_second_decimal(self) -> None:
        raw_value = 123456789
        token = TokenFactory(symbol='CAT', decimals=6)
        token_with_amount = TokenWithAmount(token, raw_value)

        self.assertEqual(token_with_amount.adjusted_amount, 123.45)
        
    def test_truncation_function_truncates_at_the_second_digit(self) -> None:
        raw_value = 12345.6789
        truncated_value = TokenWithAmount.truncate_adjusted_amount_at_second_digit(raw_value)

        self.assertEqual(truncated_value, 12345.67)
