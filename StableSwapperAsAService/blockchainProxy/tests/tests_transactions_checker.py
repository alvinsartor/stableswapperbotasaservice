# Django
from django.test import TestCase

# Local
from blockchainProxy.transactions_checker import TransactionsChecker
from blockchainProxy.tests.transactions_receipts_for_tests import SUCCESSFUL_TRANSACTION_RECEIPT
from kernel.factories import TokenFactory


class TransactionsCheckerTest(TestCase):

    def test_tx_checker_detects_successful_transactions(self) -> None:
        tx_receipt = dict(SUCCESSFUL_TRANSACTION_RECEIPT)
        is_successful: bool = TransactionsChecker.was_transaction_executed(tx_receipt)

        self.assertTrue(is_successful)

    def test_tx_checker_detects_unsuccessful_transactions(self) -> None:
        tx_receipt = dict(SUCCESSFUL_TRANSACTION_RECEIPT)
        tx_receipt['status'] = 0
        is_successful: bool = TransactionsChecker.was_transaction_executed(tx_receipt)

        self.assertFalse(is_successful)

    def test_tx_checker_retrieves_transaction_block(self) -> None:
        tx_receipt = dict(SUCCESSFUL_TRANSACTION_RECEIPT)
        tx_block: int = TransactionsChecker.get_transaction_block(tx_receipt)

        self.assertEqual(tx_block, 17876096)

    def test_tx_checker_retrieves_gas_used(self) -> None:
        tx_receipt = dict(SUCCESSFUL_TRANSACTION_RECEIPT)
        gas_used: int = TransactionsChecker.get_transaction_gas_used(tx_receipt)

        self.assertEqual(gas_used, 729973)

    def test_tx_checker_retrieves_swap_profit(self) -> None:
        TokenFactory.add_all_tokens()
        tx_receipt = dict(SUCCESSFUL_TRANSACTION_RECEIPT)
        swap_profit: int = TransactionsChecker.get_swap_profit(tx_receipt)

        self.assertAlmostEqual(swap_profit, 0.28)