
# Django
from django.test import TestCase

# Local
from blockchainProxy.token_with_amount import TokenWithAmount
from blockchainProxy.quote_with_tx_data import QuoteWithTxData
from kernel.factories import TokenFactory


class QuoteWithTxDataTest(TestCase):

    def test_minimum_granted_depends_on_slippage(self) -> None:
        token = TokenFactory(symbol='CAT', decimals=6)
        token_with_amount = TokenWithAmount(token, 123000000)
        quote_with_tx_data = QuoteWithTxData(token_with_amount, 1, {})

        self.assertEqual(quote_with_tx_data.minimum_granted, 121.77) # 123.00 - 1%
