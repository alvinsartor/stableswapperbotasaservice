# Generated by Django 3.2.6 on 2021-08-22 08:12

from django.db import migrations, models
import statistics.models.wallet_daily_statistic


class Migration(migrations.Migration):

    dependencies = [
        ('statistics', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='walletdailystatistic',
            name='date',
            field=models.DateField(default=statistics.models.wallet_daily_statistic.today),
        ),
    ]
