# Generated by Django 3.2.5 on 2021-08-13 19:03

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('kernel', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='JournalEntry',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('date', models.DateField()),
                ('title', models.CharField(max_length=256)),
                ('goal', models.CharField(blank=True, default=None, max_length=256, null=True)),
                ('additional_info', models.TextField(blank=True, default=None, null=True)),
            ],
        ),
        migrations.CreateModel(
            name='WalletDailyStatistic',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('date', models.DateField(auto_now_add=True)),
                ('number_checks', models.IntegerField()),
                ('number_non_aborted_checks', models.IntegerField()),
                ('number_erroneous_checks', models.IntegerField()),
                ('average_check_duration', models.FloatField()),
                ('number_transactions', models.IntegerField()),
                ('number_successful_transactions', models.IntegerField()),
                ('number_transactions_with_error', models.IntegerField()),
                ('number_transactions_not_found', models.IntegerField()),
                ('total_delta_profit_in_transactions', models.FloatField()),
                ('average_delta_profit_in_transactions', models.FloatField()),
                ('total_forecasted_profit_in_transactions', models.FloatField()),
                ('average_forecasted_profit_in_transactions', models.FloatField()),
                ('count_positive_transactions', models.IntegerField()),
                ('amount_positive_transactions', models.FloatField()),
                ('count_negative_transactions', models.IntegerField()),
                ('amount_negative_transactions', models.FloatField()),
                ('total_fees', models.FloatField()),
                ('apr', models.FloatField()),
                ('daily_apr', models.FloatField()),
                ('weekly_apr', models.FloatField()),
                ('weekly_profit', models.FloatField()),
                ('total_owned', models.FloatField()),
                ('wallet', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='kernel.wallet')),
            ],
        ),
    ]
