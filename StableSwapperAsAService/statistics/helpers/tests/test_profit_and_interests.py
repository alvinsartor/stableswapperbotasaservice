# Python
from datetime import timedelta
from unittest.mock import patch, Mock

# Django
from django.utils import timezone
from django.test import TestCase

# Local
from kernel.models import Transaction
from kernel.factories import TokenFactory
from kernel.factories import TransactionFactory
from kernel.factories import WalletFactory
from statistics.helpers.profit_and_interests import ProfitAndInterests


class ProfitAndInterestsTest(TestCase):

    def setUp(self) -> None:
        self.wallet = WalletFactory()
        TokenFactory.add_all_tokens()
        
    def test_initial_data_is_the_data_of_the_first_transaction(self) -> None:        
        # Create a transaction that is 1 week old
        last_week = timezone.now() - timedelta(weeks=1)
        with patch('django.utils.timezone.now', Mock(return_value=last_week)):
            TransactionFactory(wallet=self.wallet)

        # And a bunch of other txs
        TransactionFactory.create_batch(5, wallet=self.wallet)

        profit_and_interests = ProfitAndInterests(Transaction.objects.all(), self.wallet)
        self.assertEqual(profit_and_interests.initial_date, last_week)
    
    def test_total_profit_is_the_sum_of_profits_of_the_transactions(self) -> None:
        txs = TransactionFactory.create_batch(5, wallet=self.wallet)
        expected_profit = sum([tx.profit for tx in txs])

        profit_and_interests = ProfitAndInterests(Transaction.objects.all(), self.wallet)
        self.assertEqual(profit_and_interests.total_profit, expected_profit)
                
    def test_total_owned_is_the_amount_of_tokens_currently_owned(self) -> None:
        TransactionFactory.create_batch(5, wallet=self.wallet)        
        
        with patch('blockchainProxy.wallet_checker.WalletChecker.get_total_owned', Mock(return_value=1250.25)):        
            profit_and_interests = ProfitAndInterests(Transaction.objects.all(), self.wallet)
        
        self.assertEqual(profit_and_interests.total_owned, 1250.25)
                        
    def test_initially_owned_is_the_amount_of_tokens_currently_owned_minus_the_profit(self) -> None:
        txs = TransactionFactory.create_batch(5, wallet=self.wallet)
        
        with patch('blockchainProxy.wallet_checker.WalletChecker.get_total_owned', Mock(return_value=1250.25)):        
            profit_and_interests = ProfitAndInterests(Transaction.objects.all(), self.wallet)
        
        txs_profit = sum([tx.profit for tx in txs])
        self.assertEqual(profit_and_interests.initially_owned, 1250.25 - txs_profit)
        
    def test_profit_percentage_is_calculated_correctly(self) -> None:
        # Create transactions (the first one is a week old)
        last_week = timezone.now() - timedelta(weeks=1)
        with patch('django.utils.timezone.now', Mock(return_value=last_week)):
            TransactionFactory(wallet=self.wallet, profit=1)
        TransactionFactory.create_batch(9, wallet=self.wallet, profit=1)
        self.assertEqual(sum([tx.profit for tx in Transaction.objects.all()]), 10)

        with patch('blockchainProxy.wallet_checker.WalletChecker.get_total_owned', Mock(return_value=1010)):        
            profit_and_interests = ProfitAndInterests(Transaction.objects.all(), self.wallet)

        self.assertEqual(profit_and_interests.profit_percentage, 1)
        
    def test_intrests_are_calculated_correctly(self) -> None:
        # Create transactions (the first one is a week old)
        last_week = timezone.now() - timedelta(weeks=1)
        with patch('django.utils.timezone.now', Mock(return_value=last_week)):
            TransactionFactory(wallet=self.wallet, profit=1)
        TransactionFactory.create_batch(9, wallet=self.wallet, profit=1)
        self.assertEqual(sum([tx.profit for tx in Transaction.objects.all()]), 10)

        with patch('blockchainProxy.wallet_checker.WalletChecker.get_total_owned', Mock(return_value=1010)):        
            profit_and_interests = ProfitAndInterests(Transaction.objects.all(), self.wallet)

        self.assertAlmostEqual(profit_and_interests.daily_APR, 1 / 7, 4)
        self.assertAlmostEqual(profit_and_interests.APR, 1 / 7 * 365, 4)
        self.assertAlmostEqual(profit_and_interests.APY, 68.380556702, 4)