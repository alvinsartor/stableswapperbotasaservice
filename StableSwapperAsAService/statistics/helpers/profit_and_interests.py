# Python
from typing import Dict, Any, List
import math

# Django
from django.utils import timezone
from django.db.models import QuerySet

# Local
from blockchainProxy.wallet_checker import WalletChecker
from kernel.models import Transaction
from kernel.models import Wallet
from bot.models import Check

class ProfitAndInterests:
    """ 
        Object grouping a bunch of values relative to profits and interests 
        relative to a list of transactions.    
    """
    
    def __init__(self, transactions: QuerySet, wallet: Wallet):
        transactions = list(transactions.order_by('date'))
        
        self.total_owned = WalletChecker.get_total_owned(wallet.address)
    
        if not transactions:
            self.initial_date = timezone.now()
            self.total_profit = 0
            self.profit_percentage = 0
            self.initially_owned = self.total_owned
            
            self.daily_APR = 0
            self.APR = 0
            self.APY = 0
            return
        
        self.initial_date = transactions[0].date
        self.total_profit = sum([tx.profit for tx in transactions if tx.profit])
        self.initially_owned = self.total_owned - self.total_profit
        self.profit_percentage = 100 * self.total_profit / self.initially_owned
        
        days_since_initial: float = (timezone.now() - self.initial_date).total_seconds() / 86400

        self.daily_APR = self.profit_percentage / days_since_initial
        self.APR = self.daily_APR * 365
        self.APY = 0
        
        try:
            self.APY = (math.pow(1 + self.daily_APR / 100, 365) - 1) * 100
        except OverflowError:
            self.APY = float("inf")
            
