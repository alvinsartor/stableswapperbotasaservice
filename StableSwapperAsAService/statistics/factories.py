# Python
from datetime import timedelta

# Django
from django.utils import timezone

# 3rd Party
import factory
from factory import fuzzy

# Local
from kernel.factories import WalletFactory
from statistics.models import WalletDailyStatistic
from statistics.models import JournalEntry


class WalletDailyStatisticFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = WalletDailyStatistic

    wallet = factory.SubFactory(WalletFactory)
    number_checks = fuzzy.FuzzyInteger(2000, high=3000)
    number_non_aborted_checks = fuzzy.FuzzyInteger(100, high=200)
    number_erroneous_checks = fuzzy.FuzzyInteger(50, high=250)
    average_check_duration = fuzzy.FuzzyFloat(2.0, high=15.0)

    number_transactions = fuzzy.FuzzyInteger(50, high=300)
    number_successful_transactions = fuzzy.FuzzyInteger(40, high=150)
    number_transactions_with_error = fuzzy.FuzzyInteger(40, high=150)
    number_transactions_not_found = fuzzy.FuzzyInteger(20, high=100)
    number_waiting_transactions = fuzzy.FuzzyInteger(1, high=5)
    total_delta_profit_in_transactions = fuzzy.FuzzyFloat(2.0, high=15.0)
    average_delta_profit_in_transactions = factory.LazyAttribute(
        lambda x: x.total_delta_profit_in_transactions / x.number_successful_transactions
    )
    total_forecasted_profit_in_transactions = fuzzy.FuzzyFloat(3.0, high=25.0)
    average_forecasted_profit_in_transactions = factory.LazyAttribute(
        lambda x: x.total_forecasted_profit_in_transactions / x.number_successful_transactions
    )

    count_positive_transactions = fuzzy.FuzzyInteger(25, high=125)
    amount_positive_transactions = fuzzy.FuzzyFloat(5.0, high=32.0)
    count_negative_transactions = fuzzy.FuzzyInteger(0, high=25)
    amount_negative_transactions = fuzzy.FuzzyFloat(0.0, high=16.0)
    
    volumes = fuzzy.FuzzyFloat(5000, 15000)
    total_fees = fuzzy.FuzzyFloat(1.0, high=3.0)
    topup_amounts = fuzzy.FuzzyFloat(1.0, high=4.0)

    apr = factory.LazyAttribute(lambda x: x.daily_apr * 365)
    daily_apr = fuzzy.FuzzyFloat(0.25, high=1.5)
    weekly_apr = factory.LazyAttribute(lambda x: x.daily_apr * 7)
    weekly_profit = fuzzy.FuzzyFloat(1000, high=1500)

    total_owned = fuzzy.FuzzyFloat(500, high=775)


POSSIBLE_GOALS = ['General Improvement', 'Reduce Errors', 'Increase Happiness']


class JournalEntryFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = JournalEntry

    date = fuzzy.FuzzyDate(timezone.now().date() - timedelta(days=30))
    title = factory.Faker('sentence')
    goal = fuzzy.FuzzyChoice(POSSIBLE_GOALS)
    additional_info = factory.Faker('sentence')
