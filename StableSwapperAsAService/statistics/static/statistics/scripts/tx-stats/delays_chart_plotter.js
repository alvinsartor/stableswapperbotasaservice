
async function plotDeltaChart()
{
    const canvas = document.getElementById('delays-chart');
    const rawData = await $.getJSON('/statistics/data/transactions_delays/');
    
    Chart.defaults.plugins.legend.display = false;
    Chart.defaults.responsive = true;
    Chart.defaults.maintainAspectRatio = false;

    new Chart(canvas, {
        type: 'bar',
        data: {
            labels: rawData.labels,
            datasets: [{
                data: rawData.data,
                fill: false,
                backgroundColor: 'rgba(82, 136, 163, 0.5)',
                borderColor: 'rgb(82, 136, 163)',
                borderWidth: 1,
            }],
        },
    });
} 

plotDeltaChart();