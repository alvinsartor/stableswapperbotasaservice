
async function plotDeltaChart()
{
    const canvas = document.getElementById('gas-price-chart');
    const rawData = await $.getJSON('/statistics/data/transactions_gas_price/');
    
    Chart.defaults.plugins.legend.display = false;
    Chart.defaults.responsive = true;
    Chart.defaults.maintainAspectRatio = false;

    new Chart(canvas, {
        type: 'bar',
        data: {
            labels: rawData.labels,
            datasets: [{
                data: rawData.data,
                fill: false,
                backgroundColor: 'rgba(245, 184, 0, 0.5)',
                borderColor: 'rgb(245, 184, 0)',
                borderWidth: 1,
            }],
        },
    });
} 

plotDeltaChart();