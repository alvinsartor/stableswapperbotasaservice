
async function plotDeltaChart()
{
    const canvas = document.getElementById('slippage-chart');
    const rawData = await $.getJSON('/statistics/data/transactions_slippage/');
    
    Chart.defaults.plugins.legend.display = false;
    Chart.defaults.responsive = true;
    Chart.defaults.maintainAspectRatio = false;

    new Chart(canvas, {
        type: 'bar',
        data: {
            labels: rawData.labels,
            datasets: [{
                data: rawData.data,
                fill: false,
                backgroundColor: 'rgba(112, 88, 116, 0.5)',
                borderColor: 'rgb(112, 88, 116)',
                borderWidth: 1,
            }],
        },
    });
} 

plotDeltaChart();