
async function plotDeltaChart()
{
    const canvas = document.getElementById('runners-failures-chart');
    const rawData = await $.getJSON('/statistics/data/runners_failures_over_time/');
    
    Chart.defaults.plugins.legend.display = false;
    Chart.defaults.responsive = true;
    Chart.defaults.maintainAspectRatio = false;

    new Chart(canvas, {
        type: 'bar',
        data: {
            labels: rawData.labels.reverse(),
            datasets: [{
                data: rawData.data.reverse(),
                fill: false,
                backgroundColor: 'rgba(191, 26, 47, 0.5)',
                borderColor: 'rgb(191, 26, 47)',
                borderWidth: 1,
            }],
        },
    });
} 

plotDeltaChart();