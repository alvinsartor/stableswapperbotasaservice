
async function plotChecksTrendsChart()
{
    const canvas = document.getElementById('checks-trend-chart');
    const rawData = await $.getJSON('/statistics/data/checks_trends_over_time/');
    
    Chart.defaults.plugins.legend.display = false;
    Chart.defaults.responsive = true;
    Chart.defaults.maintainAspectRatio = false;

    new Chart(canvas, {
        type: 'line',
        data: {
            labels: rawData.labels,
            datasets: [
                {
                    data: rawData.data_total,
                    fill: false,
                    borderColor: 'rgb(252, 129, 74)',
                    tension: 0.1
                },
                {
                    data: rawData.data_successful,
                    fill: false,
                    borderColor: 'rgb(221, 72, 3)',
                    tension: 0.1
                }
            ],
        },        
    });
} 

plotChecksTrendsChart();