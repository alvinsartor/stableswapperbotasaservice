
async function plotChecksDurationChart()
{
    const canvas = document.getElementById('checks-durations-chart');
    const rawData = await $.getJSON('/statistics/data/checks_durations_over_time/');
    
    Chart.defaults.plugins.legend.display = false;
    Chart.defaults.responsive = true;
    Chart.defaults.maintainAspectRatio = false;

    new Chart(canvas, {
        type: 'line',
        data: {
            labels: rawData.labels,
            datasets: [{
                data: rawData.data,
                fill: false,
                borderColor: 'rgb(82, 136, 163)',
                tension: 0.1
            }],
        },        
    });
} 

plotChecksDurationChart();