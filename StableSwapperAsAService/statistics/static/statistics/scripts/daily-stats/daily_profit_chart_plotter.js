
async function plotDailyProfit()
{
    const canvas = document.getElementById('daily-profit-chart');
    const rawData = await $.getJSON('/statistics/data/daily_profit_over_time/');
    
    Chart.defaults.plugins.legend.display = false;
    Chart.defaults.responsive = true;
    Chart.defaults.maintainAspectRatio = false;

    new Chart(canvas, {
        type: 'bar',
        data: {
            labels: rawData.labels,
            datasets: [{
                data: rawData.data,
                fill: false,
                backgroundColor: 'rgba(0, 163, 82, 0.5)',
                borderColor: 'rgb(0, 163, 82)',
                borderWidth: 1,
            }],
        },
    });
} 

plotDailyProfit();