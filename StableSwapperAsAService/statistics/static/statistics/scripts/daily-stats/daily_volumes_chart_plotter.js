
async function plotDailyProfit()
{
    const canvas = document.getElementById('volumes-chart');
    const rawData = await $.getJSON('/statistics/data/volumes/');
    
    Chart.defaults.plugins.legend.display = false;
    Chart.defaults.responsive = true;
    Chart.defaults.maintainAspectRatio = false;

    new Chart(canvas, {
        type: 'bar',
        data: {
            labels: rawData.labels,
            datasets: [{
                data: rawData.data,
                fill: false,
                backgroundColor: 'rgba(0, 166, 118, 0.5)',
                borderColor: 'rgb(0, 166, 118)',
                borderWidth: 1,
            }],
        },
    });
} 

plotDailyProfit();