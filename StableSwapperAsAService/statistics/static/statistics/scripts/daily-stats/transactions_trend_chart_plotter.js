
async function plotTransactionsTrendsChart()
{
    const canvas = document.getElementById('transactions-trend-chart');
    const rawData = await $.getJSON('/statistics/data/transactions_trends_over_time/');
    
    Chart.defaults.plugins.legend.display = false;
    Chart.defaults.responsive = true;
    Chart.defaults.maintainAspectRatio = false;

    new Chart(canvas, {
        type: 'line',
        data: {
            labels: rawData.labels,
            datasets: [
                {
                    data: rawData.data_total,
                    fill: false,
                    borderColor: 'rgb(194, 71, 88)',
                    tension: 0.1
                },
                {
                    data: rawData.data_successful,
                    fill: false,
                    borderColor: 'rgb(107, 36, 45)',
                    tension: 0.1
                }
            ],
        },        
    });
} 

plotTransactionsTrendsChart();