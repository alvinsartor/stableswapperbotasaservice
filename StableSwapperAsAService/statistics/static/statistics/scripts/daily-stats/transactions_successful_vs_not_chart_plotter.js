
async function plotTransactionsTrendsChart()
{
    const canvas = document.getElementById('transactions-successful-vs-not-chart');
    const rawData = await $.getJSON('/statistics/data/transactions_successful_vs_not/');
    
    Chart.defaults.plugins.legend.display = false;
    Chart.defaults.responsive = true;
    Chart.defaults.maintainAspectRatio = false;

    new Chart(canvas, {
        type: 'bar',
        data: {
            labels: rawData.labels,
            datasets: [
                {
                    data: rawData.percentage_successful,
                    fill: true,
                    borderColor: 'rgb(53, 192, 115)',
                    backgroundColor: 'rgba(53, 192, 115)',
                    tension: 0.1
                },
                {
                    data: rawData.percentage_not_found,
                    fill: true,
                    borderColor: 'rgb(240, 45, 101)',
                    backgroundColor: 'rgba(240, 45, 101)',
                    tension: 0.1
                },
                {
                    data: rawData.percentage_failed,
                    fill: true,
                    borderColor: 'rgb(155, 29, 32)',
                    backgroundColor: 'rgba(155, 29, 32)',
                    tension: 0.1
                }
            ],
        },        
    });
} 

plotTransactionsTrendsChart();