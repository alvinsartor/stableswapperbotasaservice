
async function plotTransactionsTrendsChart()
{
    const canvas = document.getElementById('transactions-positive-vs-negative-count-chart');
    const rawData = await $.getJSON('/statistics/data/transactions_positive_vs_negative_count/');
    
    Chart.defaults.plugins.legend.display = false;
    Chart.defaults.responsive = true;
    Chart.defaults.maintainAspectRatio = false;

    new Chart(canvas, {
        type: 'line',
        data: {
            labels: rawData.labels,
            datasets: [
                {
                    data: rawData.data_positive,
                    fill: true,
                    borderColor: 'rgb(4, 167, 119)',
                    backgroundColor: 'rgba(4, 167, 119, 0.1)',
                    tension: 0.1
                },
                {
                    data: rawData.data_negative,
                    fill: true,
                    borderColor: 'rgb(202, 69, 63)',
                    backgroundColor: 'rgba(202, 69, 63, 0.1)',
                    tension: 0.1
                }
            ],
        },        
    });
} 

plotTransactionsTrendsChart();