
async function plotDailyProfit()
{
    const canvas = document.getElementById('topup-amounts-chart');
    const rawData = await $.getJSON('/statistics/data/topup_amounts/');
    
    Chart.defaults.plugins.legend.display = false;
    Chart.defaults.responsive = true;
    Chart.defaults.maintainAspectRatio = false;

    new Chart(canvas, {
        type: 'bar',
        data: {
            labels: rawData.labels,
            datasets: [{
                data: rawData.data,
                fill: false,
                backgroundColor: 'rgba(216, 87, 42, 0.5)',
                borderColor: 'rgb(216, 87, 42)',
                borderWidth: 1,
            }],
        },
    });
} 

plotDailyProfit();