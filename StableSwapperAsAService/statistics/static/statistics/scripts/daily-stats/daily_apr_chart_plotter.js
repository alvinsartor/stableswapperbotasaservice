
async function plotDailyAprChart()
{
    const canvas = document.getElementById('apy-trend-chart');
    const rawData = await $.getJSON('/statistics/data/daily_apr_over_time/');
    
    Chart.defaults.plugins.legend.display = false;
    Chart.defaults.responsive = true;
    Chart.defaults.maintainAspectRatio = false;

    new Chart(canvas, {
        type: 'line',
        data: {
            labels: rawData.labels.reverse(),
            datasets: [{
                data: rawData.data.reverse(),
                fill: false,
                borderColor: 'rgb(75, 192, 192)',
                tension: 0.1
            }],
        },        
    });
} 

plotDailyAprChart();