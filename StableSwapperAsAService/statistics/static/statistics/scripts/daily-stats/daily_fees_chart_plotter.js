
async function plotChecksDurationChart()
{
    const canvas = document.getElementById('daily-fees-trend-chart');
    const rawData = await $.getJSON('/statistics/data/daily_fees/');
    
    Chart.defaults.plugins.legend.display = false;
    Chart.defaults.responsive = true;
    Chart.defaults.maintainAspectRatio = false;

    new Chart(canvas, {
        type: 'line',
        data: {
            labels: rawData.labels,
            datasets: [{
                data: rawData.data,
                fill: false,
                borderColor: '#cc9900',
                tension: 0.1
            }],
        },        
    });
} 

plotChecksDurationChart();