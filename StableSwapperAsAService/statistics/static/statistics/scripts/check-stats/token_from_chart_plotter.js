
async function plotDeltaChart()
{
    const token_symbol_to_color = {
        'USDC': '#2775CA',
        'USDT': '#53AE94',
        'DAI': '#FAB62C',
        'FRAX': '#DDD',
        'MAI': '#CA453F',
        'mUSD': '#000',
        'rUSD': '#0d285a',
    }

    const canvas = document.getElementById('token-from-pie-chart');
    const rawData = await $.getJSON('/statistics/data/tokens_owned/');
    
    Chart.defaults.plugins.legend.display = false;
    Chart.defaults.responsive = true;
    Chart.defaults.maintainAspectRatio = false;

    const sortedColors = rawData.labels.map(label => token_symbol_to_color[label] || '#6d2878');

    new Chart(canvas, {
        type: 'doughnut',
        data: {
            labels: rawData.labels,
            datasets: [{
                data: rawData.data,
                fill: false,
                backgroundColor: sortedColors,
                borderWidth: 1,
            }],
        },
    });
} 

plotDeltaChart();
document.getElementById('token-from-pie-chart').parentElement.style.height = "250px";