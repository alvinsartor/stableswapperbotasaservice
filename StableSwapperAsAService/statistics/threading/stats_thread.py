# Python
import threading

# Django
from django.conf import settings
from django.urls import reverse

# 3rd Party
import requests

# Local
from kernel.models import Wallet
from statistics.models import WalletDailyStatistic


class StatisticsThread(threading.Thread):

    def __init__(self, wallet: Wallet, request, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.wallet = wallet
        self.request = request

    def run(self) -> None:
        # Update stats
        WalletDailyStatistic.create_daily_statistic(self.wallet)

        # Communicate to the hive that new stats are ready to be fetched
        url = settings.HIVE_URL + '/hive/action/refresh-instance-data/'
        data = { 'instance_url': self.request.build_absolute_uri(reverse('landing-page'))[:-1] } # removing las '/' char
        headers = { 'Referer': 'StableSwapperBot' }
        response = requests.post(url, data, headers=headers)
        print(response.content)
