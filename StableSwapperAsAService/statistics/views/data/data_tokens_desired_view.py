# Python
from datetime import timedelta

# Django
from django.utils import timezone
from django.http.response import JsonResponse
from django.views.generic.base import View
from django.db.models import Count

# Local
from bot.models import Check


class DataTokensDesiredView(View):
    def get(self, request, *args, **kwargs):
        h24_ago = timezone.now() - timedelta(days=1)
        result = list(
            Check.objects.filter(date__gte=h24_ago)
            .values('token_to_swap_to__symbol')
            .annotate(total=Count('token_to_swap_to__symbol'))
            .order_by('-total')
        )

        data = [item['total'] for item in result]
        labels = [item['token_to_swap_to__symbol'] for item in result]

        return JsonResponse({'data': data, 'labels': labels})
