# Python
from datetime import timedelta
from typing import List

# Django
from django.utils import timezone
from django.http.response import JsonResponse
from django.views.generic.base import View
from django.db.models import Sum

# Local
from statistics.models import WalletDailyStatistic


class DataTransactionSuccessfulVsNotView(View):
    def get(self, request, *args, **kwargs):
        d30_ago = timezone.now() - timedelta(days=30)
        last_month_tx_stats = list(
            WalletDailyStatistic.objects
            .filter(date__gte=d30_ago)
            .values('date')
            .order_by('date')
            .annotate(successful=Sum('number_successful_transactions'))
            .annotate(not_found=Sum('number_transactions_not_found'))
            .annotate(failed=Sum('number_transactions_with_error'))
            )

        data = {
            'percentage_successful': [entry['successful'] for entry in last_month_tx_stats],
            'percentage_not_found': [entry['not_found'] for entry in last_month_tx_stats],
            'percentage_failed': [entry['failed'] for entry in last_month_tx_stats],
            'labels': [entry['date'].strftime("%d/%m %H:%M") for entry in last_month_tx_stats],
        }
        return JsonResponse(data)


