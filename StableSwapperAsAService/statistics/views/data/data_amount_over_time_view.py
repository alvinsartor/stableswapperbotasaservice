# Python
from datetime import timedelta

# Django
from django.utils import timezone
from django.http.response import JsonResponse
from django.views.generic.base import View

# Local
from statistics.models import WalletDailyStatistic


class DataAmountOverTimeView(View):
    def get(self, request, *args, **kwargs):
        wallet_address = self.kwargs.get('wallet_address', None)
        d30_ago = timezone.now() - timedelta(days=30)
        
        latest_statistics = WalletDailyStatistic.objects.filter(date__gte=d30_ago, wallet__address=wallet_address).order_by('-date')

        data = list(latest_statistics.values_list('total_owned', flat=True))
        labels = [date.strftime("%d/%m") for date in latest_statistics.values_list('date', flat=True)]

        return JsonResponse({'data': data, 'labels': labels})
