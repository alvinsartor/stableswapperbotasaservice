# Python
from datetime import timedelta
from typing import List
from datetime import timedelta, datetime
import itertools

# Django
from django.utils import timezone
from django.http.response import JsonResponse
from django.views.generic.base import View

# Local
from bot.models import ChecksRunnerSession


class DataRunnersFailuresOverTimeView(View):
    def get(self, request, *args, **kwargs):
        h48_ago = timezone.now() - timedelta(days=2)
        sessions = ChecksRunnerSession.objects.filter(shutdown_date__gte=h48_ago).order_by('-shutdown_date')

        groups = itertools.groupby(sessions, lambda c: c.shutdown_date.strftime("%d/%m %H"))

        data: List[float] = []
        labels: List[str] = []

        for group, sessions in groups:
            labels.append(group)
            data.append(sum([1 if session.error else 0 for session in sessions]))

        return JsonResponse({'data': data, 'labels': labels})
