# Python
from datetime import timedelta

# Django
from django.utils import timezone
from django.http.response import JsonResponse
from django.views.generic.base import View
from django.db.models import Sum

# Local
from statistics.models import WalletDailyStatistic


class DataTransactionTrendsOverTimeView(View):
    def get(self, request, *args, **kwargs):
        d30_ago = timezone.now() - timedelta(days=30)
        last_month_tx_stats = list(
            WalletDailyStatistic.objects
            .filter(date__gte=d30_ago)
            .values('date')
            .order_by('date')
            .annotate(number_transactions=Sum('number_transactions'))
            .annotate(number_successful_transactions=Sum('number_successful_transactions'))
            )

        data_total = [entry['number_transactions'] for entry in last_month_tx_stats]
        data_successful = [entry['number_successful_transactions'] for entry in last_month_tx_stats]
        labels = [entry['date'].strftime("%d/%m %H:%M") for entry in last_month_tx_stats]        
        return JsonResponse({'data_total': data_total, 'data_successful': data_successful, 'labels': labels})
