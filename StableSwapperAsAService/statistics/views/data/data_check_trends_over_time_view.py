# Python
from datetime import timedelta

# Django
from django.utils import timezone
from django.http.response import JsonResponse
from django.views.generic.base import View
from django.db.models import Sum

# Local
from statistics.models import WalletDailyStatistic


class DataCheckTrendsOverTimeView(View):
    def get(self, request, *args, **kwargs):
        d30_ago = timezone.now() - timedelta(days=30)
        last_month_check_stats = list(
            WalletDailyStatistic.objects
            .filter(date__gte=d30_ago)
            .values('date')
            .order_by('date')
            .annotate(number_checks=Sum('number_checks'))
            .annotate(number_non_aborted_checks=Sum('number_non_aborted_checks'))
            )

        data_total = [entry['number_checks'] for entry in last_month_check_stats]
        data_successful = [entry['number_non_aborted_checks'] for entry in last_month_check_stats]
        labels = [entry['date'].strftime("%d/%m %H:%M") for entry in last_month_check_stats]        
        return JsonResponse({'data_total': data_total, 'data_successful': data_successful, 'labels': labels})

