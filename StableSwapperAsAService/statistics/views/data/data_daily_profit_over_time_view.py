# Python
from datetime import timedelta

# Django
from django.utils import timezone
from django.http.response import JsonResponse
from django.views.generic.base import View
from django.db.models import Sum

# Local
from statistics.models import WalletDailyStatistic


class DataDailyProfitOverTimeView(View):
    def get(self, request, *args, **kwargs):
        d31_ago = timezone.now() - timedelta(days=31)
        last_month_profits = list(
            WalletDailyStatistic.objects
            .filter(date__gte=d31_ago)
            .values('date')
            .order_by('date')
            .annotate(total_profit=Sum('total_delta_profit_in_transactions'))
            )

        data = [entry['total_profit'] for entry in last_month_profits]
        labels = [entry['date'].strftime("%d/%m %H:%M") for entry in last_month_profits]
        return JsonResponse({'data': data, 'labels': labels})
