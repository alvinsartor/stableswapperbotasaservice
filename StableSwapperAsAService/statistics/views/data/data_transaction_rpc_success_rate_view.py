# Python
from datetime import timedelta
from typing import List

# Django
from django.utils import timezone
from django.http.response import JsonResponse
from django.views.generic.base import View
from django.db.models import Count, Q

# Local
from kernel.models import Transaction


class DataTransactionRpcSuccessRateView(View):
    def get(self, request, *args, **kwargs):
        txs_rpc_success_rates = list(
            Transaction.objects
            .values('endpoint_uri')
            .order_by('endpoint_uri')
            .annotate(successful=Count('state', filter=Q(state=Transaction.States.CONFIRMED)))
            .annotate(not_found=Count('state', filter=Q(state=Transaction.States.NOT_FOUND)))
            .annotate(failed=Count('state', filter=Q(state=Transaction.States.FAILED)))
            .annotate(all=Count('state'))
        )

        data = {
            'successful_count': [entry['successful']/entry['all'] for entry in txs_rpc_success_rates],
            'not_found_count': [entry['not_found']/entry['all'] for entry in txs_rpc_success_rates],
            'failed_count': [entry['failed']/entry['all'] for entry in txs_rpc_success_rates],
            'labels': [entry['endpoint_uri'] for entry in txs_rpc_success_rates],
        }
        return JsonResponse(data)


