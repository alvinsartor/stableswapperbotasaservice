# Python
from datetime import timedelta

# Django
from django.utils import timezone
from django.http.response import JsonResponse
from django.views.generic.base import View
from django.db.models import Avg

# Local
from statistics.models import WalletDailyStatistic


class DataFeesOverTimeView(View):
    def get(self, request, *args, **kwargs):
        d30_ago = timezone.now() - timedelta(days=30)
        last_month_fees = list(
            WalletDailyStatistic.objects
            .filter(date__gte=d30_ago)
            .values('date')
            .order_by('date')
            .annotate(data_point=Avg('total_fees'))
            )

        data = [entry['data_point'] for entry in last_month_fees]
        labels = [entry['date'].strftime("%d/%m %H:%M") for entry in last_month_fees]
        return JsonResponse({'data': data, 'labels': labels})
