# Python
from datetime import timedelta

# Django
from django.utils import timezone
from django.http.response import JsonResponse
from django.views.generic.base import View
from django.db.models import Sum

# Local
from statistics.models import WalletDailyStatistic


class DataTransactionSwapVolumesView(View):
    def get(self, request, *args, **kwargs):
        d30_ago = timezone.now() - timedelta(days=30)
        last_month_volumes = list(
            WalletDailyStatistic.objects
            .filter(date__gte=d30_ago)
            .values('date')
            .order_by('date')
            .annotate(volumes=Sum('volumes'))
        )

        data = [entry['volumes'] for entry in last_month_volumes]
        labels = [entry['date'].strftime("%d/%m %H:%M") for entry in last_month_volumes]
        return JsonResponse({'data': data, 'labels': labels})