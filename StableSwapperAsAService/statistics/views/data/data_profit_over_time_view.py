# Python
from typing import List
from datetime import timedelta
import itertools

# Django
from django.utils import timezone
from django.http.response import JsonResponse
from django.views.generic.base import View

# Local
from bot.models import Check


class DataProfitOverTimeView(View):
    def get(self, request, *args, **kwargs):
        h24_ago = timezone.now() - timedelta(days=1)
        checks = Check.objects.filter(forecasted_profit__isnull=False, date__gte=h24_ago).order_by('-date')

        groups = itertools.groupby(checks, lambda c: c.date.strftime("%d/%m %H"))

        data: List[float] = []
        labels: List[str] = []

        for group, checks in groups:
            labels.append(group)
            all_forecasted_profits = [check.forecasted_profit for check in checks] or [0]
            mean = sum(all_forecasted_profits) / len(all_forecasted_profits)
            data.append(mean)

        return JsonResponse({'data': data, 'labels': labels})
