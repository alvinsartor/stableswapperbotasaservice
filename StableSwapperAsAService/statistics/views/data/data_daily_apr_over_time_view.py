# Python
from datetime import timedelta

# Django
from django.utils import timezone
from django.http.response import JsonResponse
from django.views.generic.base import View
from django.db.models import Avg

# Local
from statistics.models import WalletDailyStatistic


class DataDailyAprOverTimeView(View):
    def get(self, request, *args, **kwargs):
        d30_ago = timezone.now() - timedelta(days=30)
        last_month_aprs = list(
            WalletDailyStatistic.objects
            .filter(date__gte=d30_ago)
            .values('date')
            .order_by('date')
            .annotate(data_point=Avg('daily_apr'))
            )

        data = [entry['data_point'] for entry in last_month_aprs]
        labels = [entry['date'].strftime("%d/%m %H:%M") for entry in last_month_aprs]
        return JsonResponse({'data': data, 'labels': labels})