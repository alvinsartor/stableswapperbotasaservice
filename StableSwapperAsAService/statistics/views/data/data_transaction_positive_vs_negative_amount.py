# Python
from datetime import timedelta

# Django
from django.utils import timezone
from django.http.response import JsonResponse
from django.views.generic.base import View
from django.db.models import Sum

# Local
from statistics.models import WalletDailyStatistic


class DataTransactionPositiveVsNegativeAmountView(View):
    def get(self, request, *args, **kwargs):
        d30_ago = timezone.now() - timedelta(days=30)
        last_month_tx_stats = list(
            WalletDailyStatistic.objects
            .filter(date__gte=d30_ago)
            .values('date')
            .order_by('date')
            .annotate(amount_positive_transactions=Sum('amount_positive_transactions'))
            .annotate(amount_negative_transactions=Sum('amount_negative_transactions'))
            )

        data_positive = [entry['amount_positive_transactions'] for entry in last_month_tx_stats]
        data_negative = [entry['amount_negative_transactions'] for entry in last_month_tx_stats]
        labels = [entry['date'].strftime("%d/%m %H:%M") for entry in last_month_tx_stats]
        return JsonResponse({'data_positive': data_positive, 'data_negative': data_negative, 'labels': labels})
