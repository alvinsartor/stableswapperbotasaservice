# Python
from datetime import timedelta

# Django
from django.utils import timezone
from django.http.response import JsonResponse
from django.views.generic.base import View
from django.db.models import Sum

# Local
from statistics.models import WalletDailyStatistic


class DataTransactionTopupsAmountsView(View):
    def get(self, request, *args, **kwargs):
        d30_ago = timezone.now() - timedelta(days=30)
        last_month_topups = list(
            WalletDailyStatistic.objects
            .filter(date__gte=d30_ago)
            .values('date')
            .order_by('date')
            .annotate(topup_amounts=Sum('topup_amounts'))
        )

        data = [entry['topup_amounts'] for entry in last_month_topups]
        labels = [entry['date'].strftime("%d/%m %H:%M") for entry in last_month_topups]
        return JsonResponse({'data': data, 'labels': labels})