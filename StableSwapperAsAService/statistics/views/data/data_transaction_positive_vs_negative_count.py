# Python
from datetime import timedelta

# Django
from django.utils import timezone
from django.http.response import JsonResponse
from django.views.generic.base import View
from django.db.models import Sum

# Local
from statistics.models import WalletDailyStatistic


class DataTransactionPositiveVsNegativeCountView(View):
    def get(self, request, *args, **kwargs):
        d30_ago = timezone.now() - timedelta(days=30)
        last_month_tx_stats = list(
            WalletDailyStatistic.objects
            .filter(date__gte=d30_ago)
            .values('date')
            .order_by('date')
            .annotate(count_positive_transactions=Sum('count_positive_transactions'))
            .annotate(count_negative_transactions=Sum('count_negative_transactions'))
            )

        data_positive = [entry['count_positive_transactions'] for entry in last_month_tx_stats]
        data_negative = [entry['count_negative_transactions'] for entry in last_month_tx_stats]
        labels = [entry['date'].strftime("%d/%m %H:%M") for entry in last_month_tx_stats]
        return JsonResponse({'data_positive': data_positive, 'data_negative': data_negative, 'labels': labels})
