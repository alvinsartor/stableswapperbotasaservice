# Django
from django.http.response import JsonResponse
from django.views.generic.base import View

# Local
from kernel.models import Wallet
from statistics.threading.stats_thread import StatisticsThread


class AddDailyStatisticTriggerView(View):
    def get(self, request, *args, **kwargs):

        # We delegate the creation of the statistics to separate threads
        for wallet in Wallet.objects.all():
            runner = StatisticsThread(wallet, request)
            runner.setDaemon(True)
            runner.start()
        
        return JsonResponse({"outcome": "success"})
