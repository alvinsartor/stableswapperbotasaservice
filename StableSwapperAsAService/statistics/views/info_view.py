# Python
from typing import List, Tuple, Dict, Any
from datetime import timedelta

# Django
from django.conf import settings
from django.urls import reverse
from django.utils import timezone
from django.http.response import JsonResponse
from django.views.generic.base import View

# Local
from sharedSettings.models import SharedSettings
from statistics.models import WalletDailyStatistic
from bot.views.home_view import HomeView
from blockchainProxy.wallet_checker import WalletChecker


class InfoView(View):

    def get(self, request, *args, **kwargs):         
        data: Dict[str, Any] = {}

        data['time'] = timezone.now()
        data['instance_name'] = settings.INSTANCE_NAME
        data['instance_chain'] = settings.BLOCKCHAIN_ID
        data['instance_url'] = request.build_absolute_uri(reverse('bots:home'))
        data['instance_stats'] = request.build_absolute_uri(reverse('statistics:statistics'))
        data['instance_admin'] = request.build_absolute_uri(reverse('admin:index'))
        data['wallets'] = self._get_wallets_data(request)
        data['settings'] = self._get_shared_settings()

        return JsonResponse(data)

    def _get_shared_settings(self) -> Dict[str, str]:
        settings = {s.name:s.value for s in SharedSettings.objects.all().iterator()}
        return settings

    def _get_wallets_data(self, request) -> Dict[str, Any]:
        wallets_with_stats = HomeView.get_wallets_with_statistics()
        serialized_wallets_with_stats = [w.to_dict() for w in wallets_with_stats]

        for wallet_with_stats in serialized_wallets_with_stats:
            # Add wallet URL
            local_wallet_url = reverse('bots:details', kwargs={'wallet_address': wallet_with_stats['wallet']})
            wallet_with_stats['url'] = request.build_absolute_uri(local_wallet_url)
            
            # Add owned tokens
            wallet_with_stats['owned_tokens'] = self._get_wallet_owned_tokens(wallet_with_stats['wallet'])

        return serialized_wallets_with_stats

    def _get_wallet_owned_tokens(self, wallet_address: str) -> List[Tuple[str, float, str]]:
        owned_tokens = sorted(WalletChecker.get_tokens_amounts(wallet_address), key=lambda tk: tk.adjusted_amount, reverse=True)
        owned_tokens.insert(0, WalletChecker.get_matic_balance(wallet_address))
        
        return [(tk.token.symbol, tk.adjusted_amount, tk.token.logo_uri) for tk in owned_tokens]
