# Django
from django.views.generic import ListView
from django.db.models import Count

# Local
from bot.models import ChecksRunnerSession


class SessionErrorsView(ListView):
    template_name = 'statistics/page-errors-list.html'
    context_object_name = 'errors'

    def get_queryset(self):
        checks_with_error = (
            ChecksRunnerSession.objects
            .exclude(error='')
            .exclude(error__isnull=True)
            .values('error')
            .annotate(count=Count('error'))
            .order_by('-count')
        )
        return checks_with_error