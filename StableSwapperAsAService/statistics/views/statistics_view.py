# Python
from typing import List, Tuple

# Django
from django.views.generic.base import TemplateView
from django.db.models import Count

# Local
from bot.models import Check
from bot.models import ChecksRunnerSession


class StatisticsView(TemplateView):
    template_name = 'statistics/page-statistics.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)  
        context['common_check_errors'] = self._get_common_check_errors()
        context['common_runner_errors'] = self._get_common_runner_errors()
        return context

    def _get_common_check_errors(self) -> List[Tuple[str, int]]:
        checks_with_error = (
            Check.objects
            .exclude(error='')
            .exclude(error__isnull=True)
            .values('error')
            .annotate(count=Count('error'))
            .order_by('-count')
        )

        return [[entry['error'], entry['count']] for entry in checks_with_error][:12]
    
    def _get_common_runner_errors(self) -> List[Tuple[str, int]]:
        sessions_with_error = (
            ChecksRunnerSession.objects
            .exclude(error='')
            .exclude(error__isnull=True)
            .values('error')
            .annotate(count=Count('error'))
            .order_by('-count')
        )

        return [[entry['error'], entry['count']] for entry in sessions_with_error][:12]