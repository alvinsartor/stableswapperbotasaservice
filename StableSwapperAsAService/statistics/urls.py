# Django
from django.urls import path

# Local
from statistics import views as views
from statistics.views import data as data_views
from statistics.views import actions as action_views

app_name = 'statistics'

# fmt: off
urlpatterns = [
    
    path('', views.StatisticsView.as_view(), name='statistics'),
    path('info/', views.InfoView.as_view()),
    path('check-errors-list/', views.CheckErrorsView.as_view(), name='check-errors-list'),
    path('session-errors-list/', views.SessionErrorsView.as_view(), name='session-errors-list'),

    # Actions
    path('action/add-daily-statistic/', action_views.AddDailyStatisticTriggerView.as_view()),

    # Data Home Page
    path('data/amount_over_time/<str:wallet_address>/', data_views.DataAmountOverTimeView.as_view()),

    # Data Statistics - Daily
    path('data/daily_apr_over_time/', data_views.DataDailyAprOverTimeView.as_view()),
    path('data/daily_profit_over_time/', data_views.DataDailyProfitOverTimeView.as_view()),
    path('data/checks_durations_over_time/', data_views.DataCheckDurationOverTimeView.as_view()),
    path('data/checks_trends_over_time/', data_views.DataCheckTrendsOverTimeView.as_view()),
    path('data/transactions_trends_over_time/', data_views.DataTransactionTrendsOverTimeView.as_view()),
    path('data/transactions_positive_vs_negative_count/', data_views.DataTransactionPositiveVsNegativeCountView.as_view()),
    path('data/transactions_positive_vs_negative_amount/', data_views.DataTransactionPositiveVsNegativeAmountView.as_view()),
    path('data/transactions_successful_vs_not/', data_views.DataTransactionSuccessfulVsNotView.as_view()),
    path('data/daily_fees/', data_views.DataFeesOverTimeView.as_view()),
    path('data/topup_amounts/', data_views.DataTransactionTopupsAmountsView.as_view()),
    path('data/volumes/', data_views.DataTransactionSwapVolumesView.as_view()),

    # Data Statistics - Transactions
    path('data/transactions_delays/', data_views.DataTransactionDelaysView.as_view()),
    path('data/transactions_gas_price/', data_views.DataTransactionGasPriceView.as_view()),
    path('data/transactions_slippage/', data_views.DataTransactionSlippageView.as_view()),
    path('data/transactions_rpc_success_rate/', data_views.DataTransactionRpcSuccessRateView.as_view()),

    # Data Statistics - Checks
    path('data/failures_over_time/', data_views.DataFailuresOverTimeView.as_view()),    
    path('data/profit_over_time/', data_views.DataProfitOverTimeView.as_view()),
    path('data/tokens_owned/', data_views.DataTokensOwnedView.as_view()),
    path('data/tokens_desired/', data_views.DataTokensDesiredView.as_view()),
    
    # Data Statistics - Runners
    path('data/runners_failures_over_time/', data_views.DataRunnersFailuresOverTimeView.as_view()),
]
# fmt: on
