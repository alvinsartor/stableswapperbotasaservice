# Django
from django.contrib import admin

# Local
from statistics.models import WalletDailyStatistic


@admin.register(WalletDailyStatistic)
class WalletDailyStatisticAdmin(admin.ModelAdmin):
    list_display = ('id', 'date', 'wallet')
