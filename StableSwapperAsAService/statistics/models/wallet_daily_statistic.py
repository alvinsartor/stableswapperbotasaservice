# Python
from datetime import datetime

# Django
from django.db import models
from django.db.models import QuerySet
from django.utils import timezone

# Local
from bot.models import Check, ChecksRunnerSession
from kernel.models import Transaction
from kernel.models.wallet import Wallet
from statistics.helpers.profit_and_interests import ProfitAndInterests


def today() -> datetime:
    return timezone.now().date()

class WalletDailyStatistic(models.Model):
    """Daily statistics relative to a single wallet"""

    date = models.DateField(default=today)
    wallet = models.ForeignKey(Wallet, on_delete=models.CASCADE)

    number_checks = models.IntegerField()
    number_non_aborted_checks = models.IntegerField()
    number_erroneous_checks = models.IntegerField()
    average_check_duration = models.FloatField()
    
    number_sessions = models.IntegerField()
    number_completed_sessions = models.IntegerField()
    number_erroneous_sessions = models.IntegerField()
    
    number_transactions = models.IntegerField()
    number_successful_transactions = models.IntegerField()
    number_transactions_with_error = models.IntegerField()
    number_transactions_not_found = models.IntegerField()
    number_waiting_transactions = models.IntegerField()
    
    total_delta_profit_in_transactions = models.FloatField()
    average_delta_profit_in_transactions = models.FloatField()
    total_forecasted_profit_in_transactions = models.FloatField()
    average_forecasted_profit_in_transactions = models.FloatField()
    count_positive_transactions = models.IntegerField()
    amount_positive_transactions = models.FloatField()
    count_negative_transactions = models.IntegerField()
    amount_negative_transactions = models.FloatField()
    
    volumes = models.FloatField()
    total_fees = models.FloatField()
    topup_amounts = models.FloatField()

    apr = models.FloatField()
    daily_apr = models.FloatField()
    weekly_apr = models.FloatField()
    weekly_profit = models.FloatField()

    total_owned = models.FloatField() 
    
    @classmethod
    def create_daily_statistic(cls, wallet: Wallet) -> None:
        statistic = WalletDailyStatistic()
        
        statistic.wallet = wallet
        statistic.date = timezone.now().date()
        
        midnight_of_today = statistic.date
        transactions = Transaction.objects.filter(date__gte=midnight_of_today, wallet=wallet)
        checks = Check.objects.filter(date__gte=midnight_of_today, wallet=wallet)
        sessions = ChecksRunnerSession.objects.filter(start_date__gte=midnight_of_today, wallet=wallet, shutdown_date__isnull=False)

        # Interests & Profit statistics
        profit_and_interests = ProfitAndInterests(transactions, wallet)
        statistic.apr = profit_and_interests.APR        
        statistic.daily_apr = profit_and_interests.daily_APR
        statistic.weekly_apr = profit_and_interests.profit_percentage
        statistic.weekly_profit = profit_and_interests.total_profit
        statistic.total_owned = profit_and_interests.total_owned

        # Checks statistics
        statistic.number_checks = checks.count()
        statistic.number_non_aborted_checks = checks.filter(aborted=False).count()
        statistic.number_erroneous_checks = checks.filter(failed=True).count()
        all_durations = list(checks.filter(duration_seconds__isnull=False).values_list('duration_seconds', flat=True)) or [0]
        statistic.average_check_duration = sum(all_durations) / len(all_durations)
        
        # Session statistics
        statistic.number_sessions = sessions.count()
        statistic.number_completed_sessions = sessions.filter(ran_until_completion=True).count()
        statistic.number_erroneous_sessions = sessions.exclude(error='').count()
        
        # Transactions statistics
        statistic.number_transactions = transactions.count()
        statistic.number_successful_transactions = transactions.filter(state=Transaction.States.CONFIRMED).count()
        statistic.number_transactions_with_error = transactions.filter(state=Transaction.States.FAILED).count()
        statistic.number_transactions_not_found = transactions.filter(state=Transaction.States.NOT_FOUND).count()
        statistic.number_waiting_transactions = transactions.filter(state=Transaction.States.WAITING).count()
        
        statistic.total_fees = sum([tx.fee_in_MATIC for tx in transactions])
        
        confirmed_transactions = transactions.filter(state=Transaction.States.CONFIRMED)
        statistic.volumes = sum([tx.amount_from for tx in transactions])

        confirmed_topup_transactions = confirmed_transactions.filter(type=Transaction.Type.TOPUP)
        statistic.topup_amounts = sum([tx.amount_from for tx in confirmed_topup_transactions])

        statistic.total_delta_profit_in_transactions = sum(map(lambda tx: tx.profit, confirmed_transactions))
        statistic.average_delta_profit_in_transactions = (
            statistic.total_delta_profit_in_transactions / statistic.number_successful_transactions
            if statistic.number_successful_transactions
            else 0
        )
        statistic.total_forecasted_profit_in_transactions = sum(
            map(lambda tx: tx.forecasted_profit, confirmed_transactions)
        )
        statistic.average_forecasted_profit_in_transactions = (
            statistic.total_forecasted_profit_in_transactions / statistic.number_successful_transactions
            if statistic.number_successful_transactions
            else 0
        )

        positive_transactions = [tx for tx in confirmed_transactions if tx.profit > 0]
        statistic.count_positive_transactions = len(positive_transactions)
        statistic.amount_positive_transactions = sum([tx.profit for tx in positive_transactions])

        negative_transactions = [tx for tx in confirmed_transactions if tx.profit < 0]
        statistic.count_negative_transactions = len(negative_transactions)
        statistic.amount_negative_transactions = -sum([tx.profit for tx in negative_transactions])       
        
        statistic.save()

        # Delete Pevious statistics referring to the same date
        cls.objects.filter(date=statistic.date, wallet=wallet).exclude(id=statistic.id).delete()
    