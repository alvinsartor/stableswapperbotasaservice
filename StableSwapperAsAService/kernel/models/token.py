# Django
from django.db import models
from django.core.validators import MaxValueValidator, MinValueValidator

class TokenManager(models.Manager):
    def get_queryset(self):
        return super().get_queryset().filter(swappable=True)

class AllTokensManager(models.Manager):
    def get_queryset(self):
        return super().get_queryset()

class Token(models.Model):
    symbol = models.CharField(max_length=16, blank=False, null=False, unique=True)
    decimals = models.IntegerField()
    address = models.CharField(max_length=64, blank=False, null=False)
    logo_uri = models.CharField(max_length=512, blank=True, default='')
    disabled = models.BooleanField(default=False)
    swappable = models.BooleanField(
        default=True, 
        editable=False,
        help_text="Define whether this token is a stablecoin to be swapped or it is a support token (i.e. wMATIC)",
    )
    comment = models.CharField(max_length=256, blank=True, default='')
    maximum_risk_allowed = models.FloatField(
        default=1,
        validators=[MinValueValidator(0.0), MaxValueValidator(1.0)],
        help_text="Maximum percentage of the total amount we allow this token to get to",
    )

    def __str__(self):
        return self.symbol

    objects = TokenManager()
    even_non_swappable_objects = AllTokensManager()