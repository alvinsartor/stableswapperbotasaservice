
# Django
from django.db import models
from django.core.validators import MinValueValidator

# 3rd Party
from fernet_fields import EncryptedCharField

class Wallet(models.Model):

    date_creation = models.DateTimeField(auto_now_add=True)
    address = models.CharField(max_length=45, unique=True)
    private_key = EncryptedCharField(max_length=256)
    rate_limited = models.FloatField(default=0, validators=[MinValueValidator(0.0)])