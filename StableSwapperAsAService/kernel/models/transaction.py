# Python
import math

# Django
from django.db import models

# Local
from kernel.models.token import Token
from kernel.models.wallet import Wallet


class Transaction(models.Model):

    class States(models.TextChoices):
        WAITING = 'W'
        CONFIRMED = 'C'
        FAILED = 'F'
        UNCERTAIN = 'U'
        NOT_FOUND = 'N'
        
    class Type(models.TextChoices):
        SWAP = 'S'
        TOPUP = 'T'
        ALLOWANCE = 'A'
        
    wallet = models.ForeignKey(Wallet, on_delete=models.CASCADE)
    token_from = models.ForeignKey(Token, on_delete=models.CASCADE, related_name='tx_from')
    token_to = models.ForeignKey(Token, on_delete=models.CASCADE, related_name='tx_to')
    amount_from = models.FloatField()
    amount_to = models.FloatField()
    gas_price = models.FloatField(blank=True, null=True, default=None)
    gas_used = models.FloatField(blank=True, null=True, default=None)
    slippage = models.FloatField(blank=True, null=True, default=None)
    date = models.DateTimeField(auto_now_add=True)
    state = models.CharField(max_length=1, choices=States.choices, default=States.WAITING)
    type = models.CharField(max_length=1, choices=Type.choices, default=Type.SWAP)
    address = models.CharField(max_length=80, blank=True, null=True)
    profit = models.FloatField(blank=True, null=True)
    endpoint_uri = models.CharField(max_length=256, blank=True, null=True, default=None)
    block = models.IntegerField(blank=True, null=True, default=None)
    execution_date = models.DateTimeField(blank=True, null=True, default=None)
    quotation_to_creation_delay = models.FloatField(blank=True, null=True, default=None)

    @classmethod
    def get_transactions_to_confirm(cls):
        return Transaction.objects.filter(state=Transaction.States.WAITING)

    def get_transaction_css_class_based_on_status(self) -> str:
        return 'transaction--%s' % self.get_state_display().lower().replace(' ', '-')

    @property
    def forecasted_profit(self) -> float:
        if not self.amount_to or not self.amount_from:
            return 0
        return self.amount_to - self.amount_from

    @property
    def delay_in_seconds(self) -> float:
        if not self.execution_date:
            return -1
        return (self.execution_date - self.date).total_seconds()

    @property
    def fee_in_MATIC(self) -> float:
        if not self.gas_price or not self.gas_used:
            return 0
        return (self.gas_price * self.gas_used) / math.pow(10, 9)
