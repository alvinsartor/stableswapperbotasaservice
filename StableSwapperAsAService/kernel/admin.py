# Python 
import math 
 
# Django 
from django.contrib import admin 
 
# Local 
from kernel.models import Token
from kernel.models import Transaction
from kernel.models import Wallet
 
 
@admin.register(Token) 
class TokenAdmin(admin.ModelAdmin): 
    list_display = ('symbol', 'disabled', 'maximum_risk_allowed', 'address', 'comment') 

    def get_queryset(self, request):
        return Token.even_non_swappable_objects.all()
 
 
@admin.register(Transaction) 
class TransactionAdmin(admin.ModelAdmin): 
    list_display = ('date', 'profit', 'state', 'token_from', 'token_to') 
    list_filter = ('state',) 


@admin.register(Wallet)
class WalletAdmin(admin.ModelAdmin):
    list_display = ['address']
