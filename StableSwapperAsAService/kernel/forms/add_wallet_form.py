# Django 
from django import forms
from django.core.exceptions import ValidationError

# 3rd Party
from web3 import Web3

# Local
from kernel.models import Wallet

 
#NOTE: This class is currently not used as wallets are manually added via admin
class AddWalletForm(forms.ModelForm):
    
    class Meta:
        model = Wallet
        fields = ['address', 'private_key']

    def clean(self) -> None:
        cleaned_data = super().clean()
        address = cleaned_data.get('address')

        if not Web3.isAddress(address):
             raise ValidationError("The given address does not seem to be valid")

    def clean_address(self) -> str:
        address = self.cleaned_data['address']
        return address if Web3.isChecksumAddress(address) else Web3.toChecksumAddress(address)
