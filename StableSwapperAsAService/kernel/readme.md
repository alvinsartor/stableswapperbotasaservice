# Kernel

This app contains the most basic tools that are used by the rest of the project.

### Token

Represents a ERC20 token, with address, number of decimals and logo.

### Wallet

Represents a wallet in the Polygon blockchain. It stores bot the address and the private key (encrypted) that will be used to operate on it.

### Transaction

Represents a transaction, more specifically a swap of stablecoins, on the Polygon network. 

Transactions have a state that is initialized to WAITING. The transactions are then periodically queried from the blockchain and can become CONFIRMED (if it went through) or FAILED (if an error occurred during their execution).