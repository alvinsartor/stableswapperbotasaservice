# Generated by Django 3.2.6 on 2021-08-22 08:26

from django.db import migrations, models

def add_non_swappable_MATIC_token(apps, schema_editor):   
    pass
    # MATIC is now added via the 'generate_initial_data' call

class Migration(migrations.Migration):

    dependencies = [('kernel', '0006_token_swappable')]
    operations = [migrations.RunPython(add_non_swappable_MATIC_token)]
