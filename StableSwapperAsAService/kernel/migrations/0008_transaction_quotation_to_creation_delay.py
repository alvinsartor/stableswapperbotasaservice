# Generated by Django 3.2.6 on 2021-08-23 11:24

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('kernel', '0007_datamigration_add_MATIC'),
    ]

    operations = [
        migrations.AddField(
            model_name='transaction',
            name='quotation_to_creation_delay',
            field=models.FloatField(blank=True, default=None, null=True),
        ),
    ]
