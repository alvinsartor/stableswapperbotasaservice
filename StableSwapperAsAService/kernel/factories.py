# Python
import random
from datetime import timedelta

# Django
from django.utils import timezone

# 3rd Party
import factory
from factory import fuzzy

# Local
from kernel.models import Token
from kernel.models import Transaction
from kernel.models import Wallet


class TokenFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Token

    symbol = fuzzy.FuzzyText(length=4)
    decimals = fuzzy.FuzzyChoice([6, 18])
    address = fuzzy.FuzzyText(length=32, prefix='0x')
    logo_uri = factory.Faker('url')
    maximum_risk_allowed = 0.5
    disabled = False
    comment = ''

    @staticmethod
    def add_all_tokens():
        Token.objects.all().delete()
        
        Token.objects.create(
            symbol='USDC',
            address='0x2791bca1f2de4661ed88a30c99a7a9449aa84174', 
            decimals=6, 
            logo_uri="https://tokens.1inch.exchange/0xa0b86991c6218b36c1d19d4a2e9eb0ce3606eb48.png",
            maximum_risk_allowed=0.65
        )
        Token.objects.create(
            symbol='WBUSD',
            address='0x87ff96aba480f1813af5c780387d8de7cf7d8261', 
            decimals=18, 
            logo_uri="https://assets.coingecko.com/coins/images/9576/small/BUSD.png",
            maximum_risk_allowed=0.35
        )
        Token.objects.create(
            symbol='UST',
            address='0x692597b009d13c4049a947cab2239b7d6517875f', 
            decimals=18, 
            logo_uri="https://assets.coingecko.com/coins/images/12681/small/UST.png",
            maximum_risk_allowed=0.35
        )
        Token.objects.create(
            symbol='PUSD',
            address='0x9af3b7dc29d3c4b1a5731408b6a9656fa7ac3b72', 
            decimals=18, 
            logo_uri="https://assets.coingecko.com/coins/images/16762/small/PUSD-purple-200.png",
            maximum_risk_allowed=0.35
        )
        #Token.objects.create(
        #    symbol='mUSD',
        #    address='0xe840b73e5287865eec17d250bfb1536704b43b21', 
        #    decimals=18, 
        #    logo_uri="https://tokens.1inch.exchange/0xe2f2a5c287993345a840db3b0845fbc70f5935a5.png",
        #    maximum_risk_allowed=0.5
        #)
        Token.objects.create(
            symbol='MAI',
            address='0xa3fa99a148fa48d14ed51d610c367c61876997f1', 
            decimals=18, 
            logo_uri="https://tokens.1inch.exchange/0xa3fa99a148fa48d14ed51d610c367c61876997f1.png",
            maximum_risk_allowed=0.25
        )
        Token.objects.create(
            symbol='DAI',
            address='0x8f3cf7ad23cd3cadbd9735aff958023239c6a063', 
            decimals=18, 
            logo_uri="https://tokens.1inch.exchange/0x6b175474e89094c44da98b954eedeac495271d0f.png",
            maximum_risk_allowed=0.5
        )
        Token.objects.create(
            symbol='USDT',
            address='0xc2132d05d31c914a87c6611c10748aeb04b58e8f', 
            decimals=6, 
            logo_uri="https://tokens.1inch.exchange/0xdac17f958d2ee523a2206206994597c13d831ec7.png",
            maximum_risk_allowed=0.5
        )
        Token.objects.create(
            symbol='xUSD',
            address='0x3a3e7650f8b9f667da98f236010fbf44ee4b2975', 
            decimals=18, 
            logo_uri="https://tokens.1inch.io/0x3a3e7650f8b9f667da98f236010fbf44ee4b2975.png",
            maximum_risk_allowed=0.35,
        )
        Token.objects.create(
            symbol='MATIC',
            address='0xEeeeeEeeeEeEeeEeEeEeeEEEeeeeEeeeeeeeEEeE', 
            decimals=18, 
            logo_uri="https://tokens.1inch.exchange/0x7d1afa7b718fb893db30a3abc0cfc608aacfebb0.png",
            maximum_risk_allowed=0,
            swappable=False,
            comment='MATIC token. Used only for top-ups and display. Will NOT be swapped.'
        )
        
        
class WalletFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Wallet

    address = factory.LazyAttribute(lambda x: "%032x" % random.randint(0, 0xFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF))
    private_key = "xxxxxxxxxxxx"


FAKE_RPC_URLS = ['https://quicknode.polygon.com', 'wss://moralis.polygon.com', 'https://freenode.polygon.com']

class TransactionFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Transaction

    wallet = factory.SubFactory(WalletFactory)
    token_from = factory.LazyAttribute(lambda x: Token.objects.all().order_by('?')[0])
    token_to = factory.LazyAttribute(lambda x: Token.objects.exclude(symbol=x.token_from.symbol).order_by('?')[0])
    amount_from = fuzzy.FuzzyFloat(100, high=150)
    amount_to = factory.LazyAttribute(lambda x: x.amount_from + 0.15)
    state = fuzzy.FuzzyChoice(['W', 'C', 'C', 'C', 'F', 'U', 'N'])
    type = 'S'
    profit = fuzzy.FuzzyFloat(-0.05, high=0.25)
    gas_price = fuzzy.FuzzyInteger(1000000000, high=9000000000)
    slippage = fuzzy.FuzzyFloat(0, high=1)
    block = factory.Sequence(int)
    execution_date = factory.LazyAttribute(lambda x: timezone.now() + timedelta(seconds=random.randrange(1, 25)))
    quotation_to_creation_delay = fuzzy.FuzzyFloat(0.2, high=3.0)
    endpoint_uri = fuzzy.FuzzyChoice(FAKE_RPC_URLS)
    
