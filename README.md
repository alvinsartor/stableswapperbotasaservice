# Stable Swapper Bot

Stable Swapper is a Bot that swaps stablecoins and aims at generating profit by exploiting the small volatility between them.

## Basic Idea

The goal of all Stablecoins is to maintain their own value as close as possible to the value of a currency (i.e. USD). This is obtained through a store of value (i.e. cash or similar stored somewhere) or algorithms that aim at compensate deltas through mathematical mechanism or seigniorage.

We all know, though, that the market doesn't like stability and will sprinkle entropy whenever it is possible. This causes continues small oscillations that can become quite considerable from time to time.

The bot tries to jump between a token and the other, exploting these small oscillations to gain few cents each transaction. Of course this is possible only if the transaction fees are very low and Polygon is thus the perfect platform to do so!


## Architecture

The bot has a layered architecture. More information can be found in the Readme.md of each app, but the general idea is:


![website](readme_resources/ArchitectureDiagram.png)


**Layer 0**
- SharedSettings: contains settings used across the whole application and can be modified runtime via the admin console
- Accounts: holds the information about accounts. Solely used to login in the admin panel
- Kernel: contains the most critical parts of the app, like `Tokens`, `Transactions` and `Wallets`

**Layer 1**
- AmmProxy: contains a set of functions used to communicate to the Automated Market Maker app (the one used to get quotes and swap tokens)
- BlockchainProxy: allows to query the blockchain for balances, tx statuses, etc.

**Layer 2**
- Bot: the brain of the bot. It coordinates `Runners`, checking for possible profits and triggering swaps.

**Layer 3**
- Statistics: contains statistics that can be analysed by humans to improve the bot and to have an overview of its performances.

## Screenshots

Some screenshots of the site:

![main view](readme_resources/ssb_main.png)

![statistics view](readme_resources/ssb_stats.png)

![transactions view](readme_resources/ssb_txs.png)

Mobile:

![mobile view](readme_resources/ssb_mob.jpg)
